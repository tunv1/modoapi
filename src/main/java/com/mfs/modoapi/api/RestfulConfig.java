package com.mfs.modoapi.api;

import com.mfs.modoapi.api.doctor.v1.GetDoctorDetailApiV1;
import com.mfs.modoapi.api.doctor.v1.LoginDoctorApiV1;
import com.mfs.modoapi.api.doctor.v1.OtpDoctorApiV1;
import com.mfs.modoapi.api.doctor.v1.SignUpDoctorApiV1;
import com.mfs.modoapi.api.patient.v1.*;
import org.apache.log4j.Logger;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.Set;

@ApplicationPath("api")
public class RestfulConfig extends Application {
    private final Logger logger =Logger.getLogger(getClass());

    @Override
    public Set<Class<?>> getClasses() {
        // Returns the names of the protocol versions which are
        // currently enabled for use on this connection.
        Set<Class<?>> resources = new java.util.HashSet<>();

        addRestResourceClasses(resources);
        return resources;
    }

    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(PingApi.class);
        resources.add(AppConfigApi.class);
        resources.add(ManualLoginApi.class);
        resources.add(com.mfs.modoapi.api.patient.v1.PingApi.class);

        resources.add(SignUpEmailApiV1.class);
        resources.add(LoginApiV1.class);
        resources.add(UpdateProfileApiV1.class);
        resources.add(ForgotPasswordApiV1.class);
        resources.add(ChangePasswordApiV1.class);
        resources.add(GetDoctorsApiV1.class);
        resources.add(GetDiseasesApiV1.class);
        resources.add(GetUserDetailApiV1.class);


        resources.add(GetDoctorDetailApiV1.class);
        resources.add(SignUpDoctorApiV1.class);
        resources.add(LoginDoctorApiV1.class);
        resources.add(OtpApiV1.class);
        resources.add(OtpDoctorApiV1.class);


        resources.add(SignUpPhoneApiV1.class);
        resources.add(LoginPhoneApiV1.class);
        resources.add(GetOtpRegisterApiV1.class);
        resources.add(ChangePassApiV1.class);
        resources.add(GetOtpResetPassEmailApiV1.class);
        resources.add(GetOtpResetPassPhoneApiV1.class);
        resources.add(OtpPhoneApiV1.class);
        resources.add(OtpEmailApiV1.class);
        resources.add(ResetPasswordEmailApiV1.class);
        resources.add(ResetPasswordPhoneApiV1.class);
        resources.add(LoginSkipApiV1.class);

        resources.add(GetOtpLogoutDefaultApiV1.class);
        resources.add(VerifyOtpLogoutDefaultApiV1.class);
        resources.add(ResendOtpLogoutDefaultApiV1.class);
        resources.add(LogoutApiV1.class);


        resources.add(AddUserProfileApiV1.class);
        resources.add(UpdateUserProfileApiV1.class);
        resources.add(AddUserProfileApiV1Old.class);
        resources.add(UpdateUserProfileApiV1Old.class);
        resources.add(GetUserProfileApiV1.class);
        resources.add(DeleteUserProfileApiV1.class);
        resources.add(PingMultipartApi.class);
        resources.add(GetDictionaryApiV1.class);


        resources.add(GetBalanceInfoApiV1.class);
        resources.add(FormPaymentApiV1.class);
        resources.add(CheckExamConditionApiV1.class);

        // 2022-07-01
        resources.add(PaymentResultApiV1.class);

        logger.info("Start app finished");
    }

    public static void main(String[] args) {

    }
}
