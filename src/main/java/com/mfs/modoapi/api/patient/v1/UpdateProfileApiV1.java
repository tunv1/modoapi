package com.mfs.modoapi.api.patient.v1;

import com.google.gson.JsonObject;
import com.mfs.modoapi.util.CounterRequest;
import com.mfs.modoapi.util.GeneratorSeq;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("v1")
public class UpdateProfileApiV1 {

    private static final Logger logger = Logger.getLogger(UpdateProfileApiV1.class);

    @POST
    @Path("update-profile")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response signUp(@Context HttpServletRequest request, @HeaderParam("OS") String os, String body) {

        CounterRequest.sessionCreated();
        int seqId = GeneratorSeq.getNextSeq();
        Response resApi = null;

        try {
            logger.info("SeqId: " + seqId + "| Request: " + request.getRequestURI());
            logger.info("SeqId: " + seqId + "| Request: " + request.getRequestURI() + "OS: " + os);

            logger.info("Body: " + body);


            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("status", true);
            JsonObject err = new JsonObject();
            err.addProperty("code", "E0000");
            err.addProperty("message", "OK");
            jsonObject.add("error", null);

            JsonObject userModel = new JsonObject();
            userModel.addProperty("id", 100);
            userModel.addProperty("accountType", "Email");
            userModel.addProperty("username", "Email@gmail.com");
            userModel.addProperty("fullname", "Nguyen van A");
            userModel.addProperty("avatarUrl", "https://s.meta.com.vn/img/thumb.ashx/Data/image/2021/08/03/avatar-doi-avatar-cap-avatar-doi-nguoi-that-dang-facebook-cuc-chat-25.jpg");
            userModel.addProperty("gender", 0);
            userModel.addProperty("birthday", "05-04-1999");
            userModel.addProperty("phone", "123456789");
            userModel.addProperty("address", "123456 ABC");
            userModel.addProperty("token", "asdalkjhdfalkjsdhfalkjsd");
            userModel.addProperty("call_token", "asdalkjhdfalkjsdhfalkjsd_call");

            jsonObject.add("data", userModel);



            int chao = seqId/1;

            Response response = Response.status(200).entity(jsonObject.toString()).build();
            logger.info("response: " + response.getEntity().toString());
            return response;
        } catch (Exception exception) {
            logger.error("seqid= "+seqId + "__", exception);
            exception.printStackTrace();
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("message", "Sorry-Server Error");
            return Response.status(500).entity(jsonObject.toString()).build();
        } finally {
            CounterRequest.sessionDestroy();
        }

    }
}
