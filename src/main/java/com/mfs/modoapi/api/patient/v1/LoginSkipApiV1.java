package com.mfs.modoapi.api.patient.v1;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import com.mfs.modoapi.util.*;
import com.mfs.modoapi.ws.patient.v1.CoreLoginPhoneV1;
import com.mfs.modoapi.ws.patient.v1.CoreLoginSkipV1;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("v1")
public class LoginSkipApiV1 {

    private static final Logger logger = Logger.getLogger(LoginSkipApiV1.class);

    @POST
    @Path("login-skip")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response loginPhone(   @Context                     HttpServletRequest  request,
                                  @HeaderParam("Content-Type") String              contentType,
                                  @HeaderParam("Accept")       String              accept,
                                  @HeaderParam("OS")           String              os,
                                  @HeaderParam("Version")      String              version,
                                  @HeaderParam("Token")        String              token,
                                  @HeaderParam("Language")     String              l,
                                  @HeaderParam("Timezone")     String              timezone,
                                  @HeaderParam("DeviceId")     String              deviceId,
                                  String              body       ) {
        CounterRequest.sessionCreated();
        int seqId = GeneratorSeq.getNextSeq();
        Response resApi;
        int language = 1;
        String dataModel = "userInfo";


        try {
            // log the request header
            logger.info(
                    "SeqId = "            + seqId                     +
                            "| Accept Request = " + request.getRequestURI()   +
                            "| Content-Type = "   + contentType               +
                            "| Accept = "         + accept                    +
                            "| OS = "             + os                        +
                            "| Version = "        + version                   +
                            "| Token = "          + token                     +
                            "| Language = "       + l                         +
                            "| Timezone = "       + timezone                  +
                            "| DeviceId = "       + deviceId

            );

            // log the request body
            logger.info("SeqId = " + seqId + "| Request Body = " + body.replaceAll("\r\n", ""));

            // check language
            if (!Utils.isLanguageValid(l)) {
                logger.info("SeqId = " + seqId + "| Bad app language");
                return ResponseUtils.BadRequest(language, seqId, ErrorCode.BAD_REQUEST_HEADER, request);
            } else {
                language = Utils.getLanguage(l);
            }

            // check null or empty all request header
            if (Utils.isNullOrEmpty(contentType, accept, os, version, timezone, deviceId)) {
                logger.info("SeqId = " + seqId + "| Bad header");
                return ResponseUtils.BadRequest(language, seqId, ErrorCode.BAD_REQUEST_HEADER, request);
            }

            // sign up
            CoreLoginSkipV1 coreLoginSkipV1 = new CoreLoginSkipV1();
            JsonObject reqLoginDefaultObj = new JsonObject();
            reqLoginDefaultObj.addProperty("os", os);
            reqLoginDefaultObj.addProperty("version", version);
            reqLoginDefaultObj.addProperty("languageId", language);
            reqLoginDefaultObj.addProperty("timezone", timezone);
            reqLoginDefaultObj.addProperty("deviceId", deviceId);

            resApi = WsUtils.readResult(coreLoginSkipV1.postSignupSkip(seqId, reqLoginDefaultObj));

            JsonObject joResFromCore = new Gson().fromJson(resApi.getEntity().toString(), JsonObject.class);
            logger.info("SeqId = " + seqId + "| Response from Core service = " + joResFromCore.toString().replaceAll("\r\n", ""));

            if (Utils.checkResponseFromCore(joResFromCore)) {
                String username = joResFromCore.get("username").getAsString();
                String tokenStringee = Token.genAccessToken(Constants.API_KEY_STRINGEE, Constants.SECRET_KEY_STRINGEE, Constants.EXPIRED_TOKEN_STRINGEE, username);

                String tokenMFS = Token.genAccessToken(Constants.API_KEY_MFS, Constants.SECRET_KEY_MFS, Constants.EXPIRED_TOKEN_MFS, username);

                // send token to core to save to DB
                Response responsePostUpdateToken = coreLoginSkipV1.postUpdateTokenSkip(seqId, username, tokenMFS, tokenStringee);
                resApi = WsUtils.readResult(responsePostUpdateToken);

                JsonObject joResFromCore2 = new Gson().fromJson(resApi.getEntity().toString(), JsonObject.class);

                logger.info("SeqId = " + seqId + "| Response from Core service = " + joResFromCore2.toString().replaceAll("\r\n", ""));

                if (Utils.checkResponseFromCore(joResFromCore2)) {

                } else {
                    return ResponseUtils.CoreServiceProblem(joResFromCore2,seqId,request);
                }

            } else {
                return ResponseUtils.CoreServiceProblem(joResFromCore, seqId, request);
            }

            // build response
            JsonObject jo1 = new Gson().fromJson(resApi.getEntity().toString(), JsonObject.class);
            if (Utils.checkResponseFromCore(jo1)) {
                Response response = ResponseUtils.OK(language, jo1, dataModel, resApi);
                logger.info("SeqId = " + seqId + "| Response for Request = " + request.getRequestURI() + "| Response = " + response.getEntity().toString());
                return response;
            } else {
                return ResponseUtils.CoreServiceProblem(jo1, seqId, request);
            }

        } catch (Exception exception) {
            logger.error("SeqId = "+seqId + "| Exception = " , exception);
            exception.printStackTrace();
            Response response = ResponseUtils.Exception(language);
            logger.info("SeqId = " + seqId + "| Response for Request = " + request.getRequestURI() + "| Response = " + response.getEntity().toString());
            return response;
        } finally {
            CounterRequest.sessionDestroy();
        }

    }
}
