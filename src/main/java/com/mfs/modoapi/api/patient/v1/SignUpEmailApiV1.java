package com.mfs.modoapi.api.patient.v1;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import com.mfs.modoapi.util.*;
import com.mfs.modoapi.ws.patient.v1.CoreSignUpV1;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("v1")
public class SignUpEmailApiV1 {

    private static final Logger logger = Logger.getLogger(SignUpEmailApiV1.class);

    @POST
    @Path("sign-up")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response signUp(@Context                     HttpServletRequest  request,
                           @HeaderParam("Content-Type") String              contentType,
                           @HeaderParam("Accept")       String              accept,
                           @HeaderParam("OS")           String              os,
                           @HeaderParam("Version")      String              version,
                           @HeaderParam("Token")        String              token,
                           @HeaderParam("Language")     String              l,
                           @HeaderParam("Timezone")     String              timezone,
                           @HeaderParam("DeviceId")     String              deviceId,
                           String              body       )  {

        CounterRequest.sessionCreated();
        int seqId = GeneratorSeq.getNextSeq();
        Response resApi;
        int language = 1;
        String dataModel = "data";


        try {
            // log the request header
            logger.info(
                    "SeqId = "            + seqId                     +
                            "| Accept Request = " + request.getRequestURI()   +
                            "| Content-Type = "   + contentType               +
                            "| Accept = "         + accept                    +
                            "| OS = "             + os                        +
                            "| Version = "        + version                   +
                            "| Token = "          + token                     +
                            "| Language = "       + l                         +
                            "| Timezone = "       + timezone                  +
                            "| DeviceId = "       + deviceId

            );

            // log the request body
            logger.info("SeqId = " + seqId + "| Request Body = " + body.replaceAll("\r\n", ""));

            // check language
            if (!Utils.isLanguageValid(l)) {
                logger.info("SeqId = " + seqId + "| Bad app language");
                return ResponseUtils.BadRequest(language, seqId, ErrorCode.BAD_REQUEST_HEADER, request);
            } else {
                language = Utils.getLanguage(l);
            }

            // check null or empty all request header
            if (Utils.isNullOrEmpty(contentType, accept, os, version, timezone, deviceId)) {
                logger.info("SeqId = " + seqId + "| Bad header");
                return ResponseUtils.BadRequest(language, seqId, ErrorCode.BAD_REQUEST_HEADER, request);
            }

            // check null or empty request body
            if (Utils.isNullOrEmpty(body)) {
                logger.info("SeqId = " + seqId + "| Bad body");
                return ResponseUtils.BadRequest(language, seqId, ErrorCode.NULL_REQUEST_BODY, request);
            }

            // parse request body json
            JsonObject bodyObj;
            try {
                bodyObj = new Gson().fromJson(body, JsonObject.class);
            } catch (JsonSyntaxException je) {
                logger.info("SeqId = " + seqId + "| Bad body json");
                return ResponseUtils.BadRequest(language, seqId, ErrorCode.BAD_BODY_JSON, request);

            }


            // check required body field
            JsonElement email = bodyObj.get("email");
            JsonElement password = bodyObj.get("password");
            if (Utils.isNull(email)) {
                logger.info("SeqId = " + seqId + "| Null email!");
                return ResponseUtils.BadRequest(language, seqId, ErrorCode.NULL_EMAIL, request);
            }
            if (Utils.isNull(password)) {
                logger.info("SeqId = " + seqId + "| Null password");
                return ResponseUtils.BadRequest(language, seqId, ErrorCode.NULL_PASSWORD, request);
            }

            String emailStr = email.getAsString();
            String pass  = password.getAsString();

            // check empty
            if (!Utils.checkEmailRegex(emailStr)) {
                logger.info("SeqId = " + seqId + "| Bad email!");
                return ResponseUtils.BadRequest(language, seqId, ErrorCode.BAD_EMAIL, request);
            }
            if (Utils.isNullOrEmpty(pass)) {
                logger.info("SeqId = " + seqId + "| Bad password");
                return ResponseUtils.BadRequest(language, seqId, ErrorCode.BAD_PASSWORD, request);
            }


            // prepare to call Core service
            JsonObject req = new JsonObject();
            req.add("email", bodyObj.get("email"));
            req.add("firstname", bodyObj.get("firstname"));
            req.add("lastname", bodyObj.get("lastname"));
            req.add("password", bodyObj.get("password"));
            req.addProperty("os", os);
            req.addProperty("version", version);
            req.addProperty("languageId", language);
            req.addProperty("timezone", timezone);
            req.addProperty("deviceId", deviceId);

            // call core service
            CoreSignUpV1 coreSignUpV1 = new CoreSignUpV1();
            Response res = coreSignUpV1.getUsernameSignUp(seqId, req);
            resApi = WsUtils.readResult(res);

            JsonObject joResFromCore = new Gson().fromJson(resApi.getEntity().toString(), JsonObject.class);

            logger.info("SeqId = " + seqId + "| Response from Core service = " + joResFromCore.toString().replaceAll("\r\n", ""));
            if (Utils.checkResponseFromCore(joResFromCore)) {
                String username = joResFromCore.get("username").getAsString();
                String tokenStringee = Token.genAccessToken(Constants.API_KEY_STRINGEE, Constants.SECRET_KEY_STRINGEE, Constants.EXPIRED_TOKEN_STRINGEE, username);

                String tokenMFS = Token.genAccessToken(Constants.API_KEY_MFS, Constants.SECRET_KEY_MFS, Constants.EXPIRED_TOKEN_MFS, username);

                // send token to core to save to DB
                Response responsePostUpdateToken = coreSignUpV1.postUpdateToken(seqId, username, tokenMFS, tokenStringee);
                resApi = WsUtils.readResult(responsePostUpdateToken);

                JsonObject joResFromCore2 = new Gson().fromJson(resApi.getEntity().toString(), JsonObject.class);

                logger.info("SeqId = " + seqId + "| Response from Core service = " + joResFromCore2.toString().replaceAll("\r\n", ""));

                if (Utils.checkResponseFromCore(joResFromCore2)) {

                } else {
                    return ResponseUtils.CoreServiceProblem(joResFromCore2,seqId,request);
                }

            } else {
                return ResponseUtils.CoreServiceProblem(joResFromCore,seqId,request);
            }

            // build response
            JsonObject jo1 = new Gson().fromJson(resApi.getEntity().toString(), JsonObject.class);
            if (Utils.checkResponseFromCore(jo1)) {
                JsonObject jo2 = new JsonObject();
                jo2.add("username", jo1.get("username"));
                jo2.add("otpExpiredTime", jo1.get("otpExpiredTime"));
                JsonObject jo3 = new JsonObject();
                jo3.add("data", jo2);
                logger.error("SeqId = "+seqId + "| Exception = "  + jo3.toString());
                Response response = ResponseUtils.OK(language, jo3, dataModel, resApi);
                logger.info("SeqId = " + seqId + "| Response for Request = " + request.getRequestURI() + "| Response = " + response.getEntity().toString());
                return response;
            } else {
                return ResponseUtils.CoreServiceProblem(jo1, seqId, request);
            }
        } catch (Exception exception) {
            logger.error("SeqId = "+seqId + "| Exception = " , exception);
            exception.printStackTrace();
            Response response = ResponseUtils.Exception(language);
            logger.info("SeqId = " + seqId + "| Response for Request = " + request.getRequestURI() + "| Response = " + response.getEntity().toString());
            return response;
        } finally {
            CounterRequest.sessionDestroy();
        }

    }

}
