package com.mfs.modoapi.api.patient.v1;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.mfs.modoapi.util.CounterRequest;
import com.mfs.modoapi.util.GeneratorSeq;
import com.mfs.modoapi.util.WsUtils;
import com.mfs.modoapi.ws.patient.v1.CoreGetDoctorsV1;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("v1")
public class GetDoctorsApiV1 {

    private static final Logger logger = Logger.getLogger(GetDoctorsApiV1.class);

    @GET
    @Path("getDoctors")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getDoctors(@Context HttpServletRequest request,
                               @HeaderParam("language") String language ,
                               @QueryParam("diseaseId") int diseaseId

    ) {

        CounterRequest.sessionCreated();
        int seqId = GeneratorSeq.getNextSeq();
        Response resApi = null;

        try {
            logger.info("SeqId=" + seqId + "|Accept Request=" + request.getRequestURI());

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("status", true);
            JsonObject err = new JsonObject();
            err.addProperty("code", "E0000");
            err.addProperty("message", "OK");
            jsonObject.add("error", null);

            int lang;
            if (language.equalsIgnoreCase("en")) {
                lang = 1;
            } else {
                lang = 0;
            }

            CoreGetDoctorsV1 coreGetDoctorsV1 = new CoreGetDoctorsV1();
            Response res = coreGetDoctorsV1.getDoctors(seqId, diseaseId, lang);
            resApi = WsUtils.readResult(res);

            JsonObject joResFromCore = new Gson().fromJson(resApi.getEntity().toString(), JsonObject.class);

            logger.info(resApi.getEntity().toString());

            jsonObject.add("data", joResFromCore.get("doctorDiseaseInfos"));

            Response response = Response.status(200).entity(jsonObject.toString()).build();
            logger.info("SeqId=" + seqId + "|Response for Request=" + request.getRequestURI() + "|Response=" + response.getEntity().toString());

            return response;
        } catch (Exception exception) {
            logger.error("seqid= "+seqId + "__", exception);
            exception.printStackTrace();
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("message", "Sorry-Server Error");
            return Response.status(500).entity(jsonObject.toString()).build();
        } finally {
            CounterRequest.sessionDestroy();
        }

    }
}
