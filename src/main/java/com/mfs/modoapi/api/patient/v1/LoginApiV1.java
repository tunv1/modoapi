package com.mfs.modoapi.api.patient.v1;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import com.mfs.modoapi.util.*;
import com.mfs.modoapi.util.exception.CommonException;
import com.mfs.modoapi.util.exception.CoreServiceException;
import com.mfs.modoapi.util.exception.RequiredFieldException;
import com.mfs.modoapi.ws.patient.v1.CoreLoginFacebookV1;
import com.mfs.modoapi.ws.patient.v1.CoreLoginGoogleV1;
import com.mfs.modoapi.ws.patient.v1.CoreLoginV1;
import org.apache.http.client.fluent.Form;
import org.apache.http.client.fluent.Request;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

@Path("v1")
public class LoginApiV1 {

    private static final Logger logger = Logger.getLogger(LoginApiV1.class);

    @POST
    @Path("login")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response login(@Context HttpServletRequest request,
                          @HeaderParam("Content-Type") String contentType,
                          @HeaderParam("Accept") String accept,
                          @HeaderParam("OS") String os,
                          @HeaderParam("Version") String version,
                          @HeaderParam("Token") String token,
                          @HeaderParam("Language") String l,
                          @HeaderParam("Timezone") String timezone,
                          @HeaderParam("DeviceId") String deviceId,
                          String body,
                          @QueryParam("type") String type) {
        CounterRequest.sessionCreated();
        int seqId = GeneratorSeq.getNextSeq();
        Response resApi = null;
        int language = 1;
        String dataModel = "userInfo";


        try {
            // log the request header
            logger.info(
                    "SeqId = "            + seqId                     +
                            "| Accept Request = " + request.getRequestURI()   +
                            "| Content-Type = "   + contentType               +
                            "| Accept = "         + accept                    +
                            "| OS = "             + os                        +
                            "| Version = "        + version                   +
                            "| Token = "          + token                     +
                            "| Language = "       + l                         +
                            "| Timezone = "       + timezone                  +
                            "| DeviceId = "       + deviceId

            );

            // log the request body
            logger.info("SeqId = " + seqId + "| Request Body = " + body.replaceAll("\r\n", ""));

            // check language
            if (!Utils.isLanguageValid(l)) {
                logger.info("SeqId = " + seqId + "| Bad app language");
                return ResponseUtils.BadRequest(language, seqId, ErrorCode.BAD_REQUEST_HEADER, request);
            } else {
                language = Utils.getLanguage(l);
            }

            // check null or empty all request header
            if (Utils.isNullOrEmpty(contentType, accept, os, version, timezone, deviceId)) {
                logger.info("SeqId = " + seqId + "| Bad header");
                return ResponseUtils.BadRequest(language, seqId, ErrorCode.BAD_REQUEST_HEADER, request);
            }

            // check null or empty request body
            if (Utils.isNullOrEmpty(body)) {
                logger.info("SeqId = " + seqId + "| Bad body");
                return ResponseUtils.BadRequest(language, seqId, ErrorCode.NULL_REQUEST_BODY, request);
            }

            // parse request body json
            JsonObject bodyObj;
            try {
                bodyObj = new Gson().fromJson(body, JsonObject.class);
            } catch (JsonSyntaxException je) {
                logger.info("SeqId = " + seqId + "| Bad body json");
                return ResponseUtils.BadRequest(language, seqId, ErrorCode.BAD_BODY_JSON, request);

            }


            if (type != null && type.length() > 0) {
                switch (type) {
                    case LoginType.FACEBOOK:
                        if (Utils.isNull(bodyObj.get("accessToken"))) {
                            logger.info("SeqId = "+seqId + "| Facebook accessToken required!");
                            return ResponseUtils.BadRequest(language, seqId, ErrorCode.ACCESS_TOKEN_FACEBOOK_REQUIRED, request);
                        }
                        // call facebook to get information
                        String uriFacebook = Constants.FACEBOOK_LINK_GET_USER_INFO + bodyObj.get("accessToken").getAsString() + "&fields=name,id";
                        logger.info("SeqId = "+seqId + "| Start call Facebook API: " + uriFacebook);

                        JsonObject facebookResponse = getFacebookProfile(uriFacebook, seqId);

                        logger.info("SeqId = "+seqId + "| Facebook response = " + facebookResponse);

                        if (facebookResponse == null || facebookResponse.toString().length() < 2 ||
                                facebookResponse.get("name") == null || facebookResponse.get("name").getAsString().length() < 1 ||
                                facebookResponse.get("id") == null || facebookResponse.get("id").getAsString().length() < 1 ) {
                            logger.info("SeqId = "+seqId + "| Can not get Facebook profile!");

                            return ResponseUtils.BadRequest(language, seqId, ErrorCode.CANNOT_GET_FACEBOOK_PROFILE, request);
                        }

                        JsonObject reqLoginFacebookObj = new JsonObject();
                        reqLoginFacebookObj.add("facebookId", facebookResponse.get("id"));
                        reqLoginFacebookObj.add("facebookName", facebookResponse.get("name"));
                        reqLoginFacebookObj.addProperty("os", os);
                        reqLoginFacebookObj.addProperty("version", version);
                        reqLoginFacebookObj.addProperty("languageId", language);
                        reqLoginFacebookObj.addProperty("timezone", timezone);
                        reqLoginFacebookObj.addProperty("deviceId", deviceId);

                        CoreLoginFacebookV1 coreLoginFacebookV1 = new CoreLoginFacebookV1();
                        Response response = coreLoginFacebookV1.postLoginFacebook(seqId, reqLoginFacebookObj);

                        resApi = WsUtils.readResult(response);
                        JsonObject joResFromCore = new Gson().fromJson(resApi.getEntity().toString(), JsonObject.class);
                        logger.info("SeqId = " + seqId + "| Response from Core service = " + joResFromCore.toString().replaceAll("\r\n", ""));

                        if (Utils.checkResponseFromCore(joResFromCore)) {

                            // New User
                            if (joResFromCore.get("status").getAsString().equals("0")) {
                                String username = joResFromCore.get("userInfo").getAsJsonObject().get("username").getAsString();
                                String tokenStringee = Token.genAccessToken(Constants.API_KEY_STRINGEE, Constants.SECRET_KEY_STRINGEE, Constants.EXPIRED_TOKEN_STRINGEE, username);

                                String tokenMFS = Token.genAccessToken(Constants.API_KEY_MFS, Constants.SECRET_KEY_MFS, Constants.EXPIRED_TOKEN_MFS, username);

                                // send token to core to save to DB
                                Response responsePostUpdateToken = coreLoginFacebookV1.postUpdateTokenGoogle(seqId, username, tokenMFS, tokenStringee);
                                resApi = WsUtils.readResult(responsePostUpdateToken);

                                JsonObject joResFromCore2 = new Gson().fromJson(resApi.getEntity().toString(), JsonObject.class);

                                logger.info("SeqId = " + seqId + "| Response from Core service = " + joResFromCore2.toString().replaceAll("\r\n", ""));

                                if (Utils.checkResponseFromCore(joResFromCore2)) {

                                } else {
                                    return ResponseUtils.CoreServiceProblem(joResFromCore2, seqId, request);
                                }

                                // Old User
                            }
                        } else {
                            return ResponseUtils.CoreServiceProblem(joResFromCore, seqId, request);
                        }
                        break;
                    case LoginType.GOOGLE:
                        if (Utils.isNull(bodyObj.get("accessToken"))) {
                            logger.info("SeqId = "+seqId + "| Google auth token required!");
                            return ResponseUtils.BadRequest(language, seqId, ErrorCode.ACCESS_TOKEN_GOOGLE_REQUIRED, request);
                        }

                        // call google to get information
                        // get access_token from code
                        String accessToken = getToken(bodyObj.get("accessToken").getAsString());


                        JsonObject googleResponse = getUserInfo(accessToken);

                        logger.info("SeqId = "+seqId + "| Google response = " + googleResponse);


                        if (googleResponse == null || googleResponse.toString().length() < 2 ||
                                googleResponse.get("name") == null || googleResponse.get("name").getAsString().length() < 1 ||
                                googleResponse.get("id") == null || googleResponse.get("id").getAsString().length() < 1 ) {
                            throw new CommonException(ErrorCode.CANNOT_GET_GOOGLE_PROFILE);
                        }

                        JsonObject reqLoginGoogleObj = new JsonObject();
                        reqLoginGoogleObj.add("googleId", googleResponse.get("id"));
                        reqLoginGoogleObj.add("googleName", googleResponse.get("name"));
                        reqLoginGoogleObj.addProperty("os", os);
                        reqLoginGoogleObj.addProperty("version", version);
                        reqLoginGoogleObj.addProperty("languageId", language);
                        reqLoginGoogleObj.addProperty("timezone", timezone);
                        reqLoginGoogleObj.addProperty("deviceId", deviceId);


                        CoreLoginGoogleV1 coreLoginGoogleV1 = new CoreLoginGoogleV1();
                        response = coreLoginGoogleV1.postLoginGoogle(seqId, reqLoginGoogleObj);

                        resApi = WsUtils.readResult(response);
                        joResFromCore = new Gson().fromJson(resApi.getEntity().toString(), JsonObject.class);
                        logger.info("SeqId = " + seqId + "| Response from Core service = " + joResFromCore.toString().replaceAll("\r\n", ""));

                        if (Utils.checkResponseFromCore(joResFromCore)) {

                            // New User (status=0)
                            if (joResFromCore.get("status").getAsInt() == 0) {
                                String username = joResFromCore.get("userInfo").getAsJsonObject().get("username").getAsString();
                                String tokenStringee = Token.genAccessToken(Constants.API_KEY_STRINGEE, Constants.SECRET_KEY_STRINGEE, Constants.EXPIRED_TOKEN_STRINGEE, username);

                                String tokenMFS = Token.genAccessToken(Constants.API_KEY_MFS, Constants.SECRET_KEY_MFS, Constants.EXPIRED_TOKEN_MFS, username);

                                // send token to core to save to DB
                                Response responsePostUpdateToken = coreLoginGoogleV1.postUpdateTokenGoogle(seqId, username, tokenMFS, tokenStringee);
                                resApi = WsUtils.readResult(responsePostUpdateToken);

                                JsonObject joResFromCore2 = new Gson().fromJson(resApi.getEntity().toString(), JsonObject.class);

                                logger.info("SeqId = " + seqId + "| Response from Core service = " + joResFromCore2.toString().replaceAll("\r\n", ""));

                                if (Utils.checkResponseFromCore(joResFromCore2)) {

                                } else {
                                    return ResponseUtils.CoreServiceProblem(joResFromCore, seqId, request);
                                }

                                // Old User
                            }


                        } else {
                            return ResponseUtils.CoreServiceProblem(joResFromCore, seqId, request);
                        }

                        break;
                    default:
                        break;
                }
            } else {

                // check null or bad email
                JsonElement emailObj = bodyObj.get("email");
                if (Utils.isNull(emailObj)) {
                    logger.info("SeqId = " + seqId + "| Null email!");
                    return ResponseUtils.BadRequest(language, seqId, ErrorCode.NULL_EMAIL, request);
                }
                String email = emailObj.getAsString();
                if (!Utils.checkEmailRegex(email)) {
                    logger.info("SeqId = " + seqId + "| Bad email!");
                    return ResponseUtils.BadRequest(language, seqId, ErrorCode.BAD_EMAIL, request);
                }

                // check null or bad password
                JsonElement passwordObj = bodyObj.get("password");
                if (Utils.isNull(passwordObj)) {
                    logger.info("SeqId = " + seqId + "| Null password!");
                    return ResponseUtils.BadRequest(language, seqId, ErrorCode.NULL_PASSWORD, request);
                }
                String password = passwordObj.getAsString();

                CoreLoginV1 coreLoginV1 = new CoreLoginV1();
                resApi = WsUtils.readResult(coreLoginV1.postLoginEmail(seqId, email, password));

                JsonObject joResFromCore = new Gson().fromJson(resApi.getEntity().toString(), JsonObject.class);
                logger.info("SeqId = " + seqId + "| Response from Core service = " + joResFromCore.toString().replaceAll("\r\n", ""));

                if (Utils.checkResponseFromCore(joResFromCore)) {

                } else {
                    return ResponseUtils.CoreServiceProblem(joResFromCore, seqId, request);
                }

            }

            // build response
            JsonObject jo1 = new Gson().fromJson(resApi.getEntity().toString(), JsonObject.class);
            if (Utils.checkResponseFromCore(jo1)) {
                Response response = ResponseUtils.OK(language, jo1, dataModel, resApi);
                logger.info("SeqId = " + seqId + "| Response for Request = " + request.getRequestURI() + "| Response = " + response.getEntity().toString());
                return response;
            } else {
                return ResponseUtils.CoreServiceProblem(jo1, seqId, request);
            }

        } catch (Exception exception) {
            logger.error("SeqId = "+seqId + "| Exception = " , exception);
            exception.printStackTrace();
            Response response = ResponseUtils.Exception(language);
            logger.info("SeqId = " + seqId + "| Response for Request = " + request.getRequestURI() + "| Response = " + response.getEntity().toString());
            return response;
        } finally {
            CounterRequest.sessionDestroy();
        }

    }

    public JsonObject getFacebookProfile(String urlString, int seqId) {
        URL url;
        try {
            url = new URL(urlString);
            URLConnection conn = url.openConnection();
            conn.setConnectTimeout(20000);
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            JsonObject joResFromFacebook = new Gson().fromJson(response.toString(), JsonObject.class);
            in.close();
            return joResFromFacebook;
        } catch (Exception exception) {
            logger.error("SeqId = "+seqId + "| Exception = " , exception);
            exception.printStackTrace();
            return null;
        }

    }

    // TODO ghi log, check lỗi bad request, http...
    public static String getToken(final String code) throws Exception {

        String response = Request.Post(Constants.GOOGLE_LINK_GET_TOKEN)
                .bodyForm(Form.form().add("client_id", Constants.GOOGLE_CLIENT_ID)
                        .add("client_secret", Constants.GOOGLE_CLIENT_SECRET)
                        .add("redirect_uri",Constants.GOOGLE_REDIRECT_URI)
                        .add("code", code)
                        .add("grant_type", Constants.GOOGLE_GRANT_TYPE).build())
                .execute().returnContent().asString();
        JsonObject jobj = new Gson().fromJson(response, JsonObject.class);
        return jobj.get("access_token").toString().replaceAll("\"", "");
    }

    public static JsonObject getUserInfo(final String accessToken) throws Exception{
        logger.info("S");
        String link = Constants.GOOGLE_LINK_GET_USER_INFO + accessToken;

        String response = Request.Get(link).execute().returnContent().asString();

        return new Gson().fromJson(response, JsonObject.class);
    }
}
