package com.mfs.modoapi.api.patient.v1;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import com.mfs.modoapi.util.*;
import com.mfs.modoapi.ws.patient.v1.CoreCommonV1;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("v1/wallet")
public class PaymentResultApiV1 {

    private static final Logger logger = Logger.getLogger(PaymentResultApiV1.class);

    @POST
    @Path("paymentResult")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response doPost(   @Context                     HttpServletRequest  request,
                                  @HeaderParam("Content-Type") String              contentType,
                                  @HeaderParam("Accept")       String              accept,
                                  @HeaderParam("OS")           String              os,
                                  @HeaderParam("Version")      String              version,
                                  @HeaderParam("Token")        String              token,
                                  @HeaderParam("Language")     String              l,
                                  @HeaderParam("Timezone")     String              timezone,
                                  @HeaderParam("DeviceId")     String              deviceId,
                                  String              body       ) {
        CounterRequest.sessionCreated();
        int seqId = GeneratorSeq.getNextSeq();
        Response resApi;
        int language = 1;
        String dataModel = "paymentResult";
        String corePath = "getPaymentResult";


        try {
            // log the request header
            logger.info(
                    "SeqId = "            + seqId                     +
                            "| Accept Request = " + request.getRequestURI()   +
                            "| Content-Type = "   + contentType               +
                            "| Accept = "         + accept                    +
                            "| OS = "             + os                        +
                            "| Version = "        + version                   +
                            "| Token = "          + token                     +
                            "| Language = "       + l                         +
                            "| Timezone = "       + timezone                  +
                            "| DeviceId = "       + deviceId

            );

            // log the request body
            logger.info("SeqId = " + seqId + "| Request Body = " + body.replaceAll("\r\n", ""));

            // check language
            if (!Utils.isLanguageValid(l)) {
                logger.info("SeqId = " + seqId + "| Bad app language");
                return ResponseUtils.BadRequest(language, seqId, ErrorCode.BAD_REQUEST_HEADER, request);
            } else {
                language = Utils.getLanguage(l);
            }

            // check null or empty all request header
            if (Utils.isNullOrEmpty(contentType, accept, os, version, timezone, deviceId, token)) {
                logger.info("SeqId = " + seqId + "| Bad header");
                return ResponseUtils.BadRequest(language, seqId, ErrorCode.BAD_REQUEST_HEADER, request);
            }

            // check null or empty request body
            if (Utils.isNullOrEmpty(body)) {
                logger.info("SeqId = " + seqId + "| Bad body");
                return ResponseUtils.BadRequest(language, seqId, ErrorCode.NULL_REQUEST_BODY, request);
            }

            // parse request body json
            JsonObject bodyObj;
            try {
                bodyObj = new Gson().fromJson(body, JsonObject.class);
            } catch (JsonSyntaxException je) {
                logger.info("SeqId = " + seqId + "| Bad body json");
                return ResponseUtils.BadRequest(language, seqId, ErrorCode.BAD_BODY_JSON, request);

            }

            // check required body field
            // check required field
            JsonElement requestIdE = bodyObj.get("requestId");

            if (requestIdE == null ) {
                logger.info("SeqId = " + seqId + "| requestId is null!");
                return ResponseUtils.BadRequest(language, seqId, ErrorCode.BAD_REQUEST, request);
            }

            String requestId = requestIdE.getAsString();

            if (requestId == null || requestId.length() < 1) {
                logger.info("SeqId = " + seqId + "| Bad requestId!");
                return ResponseUtils.BadRequest(language, seqId, ErrorCode.BAD_REQUEST, request);
            }

            // end check required body field

            JsonObject req = new JsonObject();
            req.addProperty("languageId", language);
            req.addProperty("requestId", requestId);
            req.addProperty("username", Token.getUsernameFromToken(token, seqId));


            CoreCommonV1 coreCommonV1 = new CoreCommonV1();
            resApi = WsUtils.readResult(coreCommonV1.postWithPath(seqId, req, corePath));

            JsonObject joResFromCore = new Gson().fromJson(resApi.getEntity().toString(), JsonObject.class);
            logger.info("SeqId = " + seqId + "| Response from Core service = " + joResFromCore.toString().replaceAll("\r\n", ""));

            if (Utils.checkResponseFromCore(joResFromCore)) {


            } else {
                return ResponseUtils.CoreServiceProblem(joResFromCore, seqId, request);

            }

            // build response
            JsonObject jo1 = new Gson().fromJson(resApi.getEntity().toString(), JsonObject.class);
            if (Utils.checkResponseFromCore(jo1)) {
                Response response = ResponseUtils.OK(language, jo1, dataModel, resApi);
                logger.info("SeqId = " + seqId + "| Response for Request = " + request.getRequestURI() + "| Response = " + response.getEntity().toString());
                return response;
            } else {
                return ResponseUtils.CoreServiceProblem(jo1, seqId, request);
            }

        } catch (Exception exception) {
            logger.error("SeqId = "+seqId + "| Exception = " , exception);
            exception.printStackTrace();
            Response response = ResponseUtils.Exception(language);
            logger.info("SeqId = " + seqId + "| Response for Request = " + request.getRequestURI() + "| Response = " + response.getEntity().toString());
            return response;
        } finally {
            CounterRequest.sessionDestroy();
        }

    }
}
