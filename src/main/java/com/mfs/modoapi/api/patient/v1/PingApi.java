package com.mfs.modoapi.api.patient.v1;

import com.google.gson.JsonObject;
import com.mfs.modoapi.util.CounterRequest;
import com.mfs.modoapi.util.GeneratorSeq;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("v1")
public class PingApi {

    private static final Logger logger = Logger.getLogger(PingApi.class);

    @GET
    @Path("ping")
    @Produces(MediaType.APPLICATION_JSON)
    public Response pingApi(@Context HttpServletRequest request) {

        CounterRequest.sessionCreated();
        int seqId = GeneratorSeq.getNextSeq();
        Response resApi = null;

        try {
            logger.info("SeqId: " + seqId + "| Request: " + request.getRequestURI());

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("result", "pong");

            int chao = seqId/1;

            Response response = Response.status(200).entity(jsonObject.toString()).build();
            logger.info(response.toString());
            return response;
        } catch (Exception exception) {
            logger.error("seqid= "+seqId + "__", exception);
            exception.printStackTrace();
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("message", "Sorry-Server Error");
            return Response.status(500).entity(jsonObject.toString()).build();
        } finally {
            CounterRequest.sessionDestroy();
        }

    }
}
