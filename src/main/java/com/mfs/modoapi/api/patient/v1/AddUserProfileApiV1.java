package com.mfs.modoapi.api.patient.v1;

import com.google.common.collect.SetMultimap;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import com.mfs.modoapi.util.*;
import com.mfs.modoapi.ws.patient.v1.CoreCommonV1;
import com.mfs.modoapi.ws.patient.v1.CoreLoginPhoneV1;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;
import org.glassfish.jersey.media.multipart.BodyPart;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.file.FileDataBodyPart;
import org.glassfish.jersey.media.multipart.file.StreamDataBodyPart;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Path("v1")
public class AddUserProfileApiV1 {

    private static final Logger logger = Logger.getLogger(AddUserProfileApiV1.class);

    @POST
    @Path("add-user-profile")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response doPost(   @Context                     HttpServletRequest  request,
                                  @HeaderParam("Content-Type") String              contentType,
                                  @HeaderParam("Accept")       String              accept,
                                  @HeaderParam("OS")           String              os,
                                  @HeaderParam("Version")      String              version,
                                  @HeaderParam("Token")        String              token,
                                  @HeaderParam("Language")     String              l,
                                  @HeaderParam("Timezone")     String              timezone,
                                  @HeaderParam("DeviceId")     String              deviceId
                                   ) {
        CounterRequest.sessionCreated();
        int seqId = GeneratorSeq.getNextSeq();
        Response resApi = null;
        int language = 1;
        String dataModel = "userInfo";
        String corePath = "add-user-profile";
        String error_code = null;
        String multipartResponse = null;
        MultipartUtility multipart = null;
        int maxImageSize = ResbundleResourceManager.getMaxFileSize();


        try {
            Map<String, String> stringMap = new HashMap<>();

            if (ServletFileUpload.isMultipartContent(request)) {
                DiskFileItemFactory factory = new DiskFileItemFactory();
                ServletFileUpload upload = new ServletFileUpload(factory);

                List<FileItem> formItems = upload.parseRequest(request);
                if (formItems != null && formItems.size() > 0) {
                    for (FileItem item : formItems) {
                        if (!item.isFormField()) {
                            if (item.getFieldName().equals("file_image")) {
                                if (item.getSize() < 1024L *1024*maxImageSize){

                                    logger.info("SeqId = " + seqId + "| File size= " + item.getSize());
                                    logger.info("SeqId = " + seqId + "| File name= " + item.getName());
                                    logger.info("SeqId = " + seqId + "| Field name= " + item.getFieldName());

                                    multipart = new MultipartUtility(ResbundleResourceManager.getMediaServerUrl() + "/api/modo/media/upload", "UTF-8", seqId);

                                    multipart.addFilePart("file", new BufferedInputStream(item.getInputStream()), item.getName(), item.getContentType());

                                } else {
                                    error_code = ErrorCode.FILE_TOO_BIG;
                                }
                            }
                        } else {
                            logger.info("SeqId = " + seqId + "| Field name = " + item.getFieldName() + "| Value = " + item.getString());
                            stringMap.put(item.getFieldName(), item.getString());
                        }
                    }
                }

            }


            // log the request header
            logger.info(
                    "SeqId = "            + seqId                     +
                            "| Accept Request = " + request.getRequestURI()   +
                            "| Content-Type = "   + contentType               +
                            "| Accept = "         + accept                    +
                            "| OS = "             + os                        +
                            "| Version = "        + version                   +
                            "| Token = "          + token                     +
                            "| Language = "       + l                         +
                            "| Timezone = "       + timezone                  +
                            "| DeviceId = "       + deviceId

            );



            // log the request body
//            logger.info("SeqId = " + seqId + "| Request Body = " + body.replaceAll("\r\n", ""));

            // check language
            if (!Utils.isLanguageValid(l)) {
                logger.info("SeqId = " + seqId + "| Bad app language");
                return ResponseUtils.BadRequest(language, seqId, ErrorCode.BAD_REQUEST_HEADER, request);
            } else {
                language = Utils.getLanguage(l);
            }

            // check null or empty all request header
            if (Utils.isNullOrEmpty(contentType, accept, os, version, timezone, deviceId, token)) {
                logger.info("SeqId = " + seqId + "| Bad header");
                return ResponseUtils.BadRequest(language, seqId, ErrorCode.BAD_REQUEST_HEADER, request);
            }

            // check required body field

            if (error_code != null && error_code.equals(ErrorCode.FILE_TOO_BIG)) {
                logger.info("SeqId = " + seqId + "| File too large ( > " + maxImageSize + "MB)");
                return ResponseUtils.BadRequest(language, seqId, ErrorCode.FILE_TOO_BIG, request);
            }

            JsonObject req = new JsonObject();
            req.addProperty("languageId", language);
            String username = Token.getUsernameFromToken(token, seqId);
            req.addProperty("username", username);

            String firstname = stringMap.get("firstname");
            String lastname = stringMap.get("lastname");
            String birthday = stringMap.get("birthday");
            String gender = stringMap.get("gender");
            String identityCard = stringMap.get("identityCard");
            String nationalityId = stringMap.get("nationalityId");
            String relationshipId = stringMap.get("relationshipId");
            String name = stringMap.get("name");
            String description = stringMap.get("description");
            String imgUrl = null;

            if (multipart != null) {
                multipart.addFormField("name", name);
                multipart.addFormField("description", description);
                multipartResponse = multipart.finish();
            }

            if (multipartResponse != null) {
                JsonObject jsonObject = new Gson().fromJson(multipartResponse, JsonObject.class);
                if (Utils.checkResponseFromCore(jsonObject)) {
                    imgUrl = jsonObject.get("imgUrl").getAsString();
                }

            }

            if (imgUrl != null) {
                req.addProperty("imgUrl", imgUrl);
            } else {
                req.add("imgUrl", null);
            }

            logger.info("SeqId = " + seqId
                    + "| username = " + username
                    + "| firstname = " + firstname
                    + "| lastname = " + lastname
                    + "| birthday = " + birthday
                    + "| gender = " + gender
                    + "| identityCard = " + identityCard
                    + "| identityCard = " + identityCard
                    + "| nationalityId = " + nationalityId
                    + "| relationshipId = " + relationshipId
                    + "| imgUrl = " + imgUrl
                    + "| name = " + name
                    + "| description = " + description
            );

            req.addProperty("firstname", firstname);
            req.addProperty("lastname", lastname);
            req.addProperty("birthday", birthday);
            if (gender != null) {
                req.addProperty("gender", Integer.valueOf(gender));
            } else {
                req.add("gender", null);
            }

            if (name != null) {
                req.addProperty("name", name);
            } else {
                req.add("name", null);
            }

            if (description != null) {
                req.addProperty("description", description);
            } else {
                req.add("description", null);
            }

            req.addProperty("identityCard", identityCard);
            if (nationalityId != null) {
                req.addProperty("nationalityId", Integer.valueOf(nationalityId));
            } else {
                req.add("nationalityId", null);
            }
            if (relationshipId != null) {
                req.addProperty("relationshipId", Integer.valueOf(relationshipId));
            } else {
                req.add("relationshipId", null);
            }



            CoreCommonV1 coreCommonV1 = new CoreCommonV1();
            resApi = WsUtils.readResult(coreCommonV1.postWithPath(seqId, req, corePath));

            JsonObject joResFromCore = new Gson().fromJson(resApi.getEntity().toString(), JsonObject.class);
            logger.info("SeqId = " + seqId + "| Response from Core service = " + joResFromCore.toString().replaceAll("\r\n", ""));

            if (Utils.checkResponseFromCore(joResFromCore)) {


            } else {
                return ResponseUtils.CoreServiceProblem(joResFromCore, seqId, request);

            }

            // build response
            JsonObject jo1 = new Gson().fromJson(resApi.getEntity().toString(), JsonObject.class);
            if (Utils.checkResponseFromCore(jo1)) {
                Response response = ResponseUtils.OK(language, jo1, dataModel, resApi);
                logger.info("SeqId = " + seqId + "| Response for Request = " + request.getRequestURI() + "| Response = " + response.getEntity().toString());
                return response;
            } else {
                return ResponseUtils.CoreServiceProblem(jo1, seqId, request);
            }

        } catch (Exception exception) {
            logger.error("SeqId = "+seqId + "| Exception = " , exception);
            exception.printStackTrace();
            Response response = ResponseUtils.Exception(language);
            logger.info("SeqId = " + seqId + "| Response for Request = " + request.getRequestURI() + "| Response = " + response.getEntity().toString());
            return response;
        } finally {
            if (resApi != null) {
                resApi.close();
            }
            CounterRequest.sessionDestroy();
        }

    }
}
