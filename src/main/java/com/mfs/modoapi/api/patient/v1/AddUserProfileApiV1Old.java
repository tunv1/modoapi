package com.mfs.modoapi.api.patient.v1;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import com.mfs.modoapi.util.*;
import com.mfs.modoapi.ws.patient.v1.CoreCommonV1;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("v1")
public class AddUserProfileApiV1Old {

    private static final Logger logger = Logger.getLogger(AddUserProfileApiV1Old.class);

    @POST
    @Path("add-user-profile-old")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response doPost(   @Context                     HttpServletRequest  request,
                                  @HeaderParam("Content-Type") String              contentType,
                                  @HeaderParam("Accept")       String              accept,
                                  @HeaderParam("OS")           String              os,
                                  @HeaderParam("Version")      String              version,
                                  @HeaderParam("Token")        String              token,
                                  @HeaderParam("Language")     String              l,
                                  @HeaderParam("Timezone")     String              timezone,
                                  @HeaderParam("DeviceId")     String              deviceId,
                                  String              body       ) {
        CounterRequest.sessionCreated();
        int seqId = GeneratorSeq.getNextSeq();
        Response resApi;
        int language = 1;
        String dataModel = "userInfo";
        String corePath = "add-user-profile";


        try {
            // log the request header
            logger.info(
                    "SeqId = "            + seqId                     +
                            "| Accept Request = " + request.getRequestURI()   +
                            "| Content-Type = "   + contentType               +
                            "| Accept = "         + accept                    +
                            "| OS = "             + os                        +
                            "| Version = "        + version                   +
                            "| Token = "          + token                     +
                            "| Language = "       + l                         +
                            "| Timezone = "       + timezone                  +
                            "| DeviceId = "       + deviceId

            );

            // log the request body
            logger.info("SeqId = " + seqId + "| Request Body = " + body.replaceAll("\r\n", ""));

            // check language
            if (!Utils.isLanguageValid(l)) {
                logger.info("SeqId = " + seqId + "| Bad app language");
                return ResponseUtils.BadRequest(language, seqId, ErrorCode.BAD_REQUEST_HEADER, request);
            } else {
                language = Utils.getLanguage(l);
            }

            // check null or empty all request header
            if (Utils.isNullOrEmpty(contentType, accept, os, version, timezone, deviceId, token)) {
                logger.info("SeqId = " + seqId + "| Bad header");
                return ResponseUtils.BadRequest(language, seqId, ErrorCode.BAD_REQUEST_HEADER, request);
            }

            // check null or empty request body
            if (Utils.isNullOrEmpty(body)) {
                logger.info("SeqId = " + seqId + "| Bad body");
                return ResponseUtils.BadRequest(language, seqId, ErrorCode.NULL_REQUEST_BODY, request);
            }

            // parse request body json
            JsonObject bodyObj;
            try {
                bodyObj = new Gson().fromJson(body, JsonObject.class);
            } catch (JsonSyntaxException je) {
                logger.info("SeqId = " + seqId + "| Bad body json");
                return ResponseUtils.BadRequest(language, seqId, ErrorCode.BAD_BODY_JSON, request);

            }

            // check required body field

            JsonObject req = new JsonObject();
            req.addProperty("languageId", language);
            req.addProperty("username", Token.getUsernameFromToken(token, seqId));
            req.add("firstname", bodyObj.get("firstname"));
            req.add("lastname", bodyObj.get("lastname"));
            req.add("birthday", bodyObj.get("birthday"));
            req.add("gender", bodyObj.get("gender"));
            req.add("identityCard", bodyObj.get("identityCard"));
            req.add("nationalityId", bodyObj.get("nationalityId"));
            req.add("relationshipId", bodyObj.get("relationshipId"));
            req.add("imgUrl", bodyObj.get("imgUrl"));


            CoreCommonV1 coreCommonV1 = new CoreCommonV1();
            resApi = WsUtils.readResult(coreCommonV1.postWithPath(seqId, req, corePath));

            JsonObject joResFromCore = new Gson().fromJson(resApi.getEntity().toString(), JsonObject.class);
            logger.info("SeqId = " + seqId + "| Response from Core service = " + joResFromCore.toString().replaceAll("\r\n", ""));

            if (Utils.checkResponseFromCore(joResFromCore)) {


            } else {
                return ResponseUtils.CoreServiceProblem(joResFromCore, seqId, request);

            }

            // build response
            JsonObject jo1 = new Gson().fromJson(resApi.getEntity().toString(), JsonObject.class);
            if (Utils.checkResponseFromCore(jo1)) {
                Response response = ResponseUtils.OK(language, jo1, dataModel, resApi);
                logger.info("SeqId = " + seqId + "| Response for Request = " + request.getRequestURI() + "| Response = " + response.getEntity().toString());
                return response;
            } else {
                return ResponseUtils.CoreServiceProblem(jo1, seqId, request);
            }

        } catch (Exception exception) {
            logger.error("SeqId = "+seqId + "| Exception = " , exception);
            exception.printStackTrace();
            Response response = ResponseUtils.Exception(language);
            logger.info("SeqId = " + seqId + "| Response for Request = " + request.getRequestURI() + "| Response = " + response.getEntity().toString());
            return response;
        } finally {
            CounterRequest.sessionDestroy();
        }

    }
}
