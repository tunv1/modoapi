package com.mfs.modoapi.api.patient.v1;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import com.mfs.modoapi.util.*;
import com.mfs.modoapi.ws.patient.v1.CoreChangePassV1;
import com.mfs.modoapi.ws.patient.v1.CoreCommonV1;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("v1")
public class GetOtpLogoutDefaultApiV1 {

    private static final Logger logger = Logger.getLogger(GetOtpLogoutDefaultApiV1.class);

    @POST
    @Path("getOtpLogoutDefault")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response otp( @Context                      HttpServletRequest  request,
                          @HeaderParam("Content-Type") String              contentType,
                          @HeaderParam("Accept")       String              accept,
                          @HeaderParam("OS")           String              os,
                          @HeaderParam("Version")      String              version,
                          @HeaderParam("Token")        String              token,
                          @HeaderParam("Language")     String              l,
                          @HeaderParam("Timezone")     String              timezone,
                          @HeaderParam("DeviceId")     String              deviceId,
                          String              body       )  {

        CounterRequest.sessionCreated();
        int seqId = GeneratorSeq.getNextSeq();
        Response resApi;
        int language = 1;
        String dataModel = "data";
        String corePath = "logout-default-get-otp";

        try {
            // log the request header
            logger.info(
                            "SeqId = "            + seqId                     +
                            "| Accept Request = " + request.getRequestURI()   +
                            "| Content-Type = "   + contentType               +
                            "| Accept = "         + accept                    +
                            "| OS = "             + os                        +
                            "| Version = "        + version                   +
                            "| Token = "          + token                     +
                            "| Language = "       + l                         +
                            "| Timezone = "       + timezone                  +
                            "| DeviceId = "       + deviceId

            );

            // log the request body
            logger.info("SeqId = " + seqId + "| Request Body = " + body.replaceAll("\r\n", ""));

            // check language
            if (!Utils.isLanguageValid(l)) {
                logger.info("SeqId = " + seqId + "| Bad app language");
                return ResponseUtils.BadRequest(language, seqId, ErrorCode.BAD_REQUEST_HEADER, request);
            } else {
                language = Utils.getLanguage(l);
            }

            // check null or empty all request header
            if (Utils.isNullOrEmpty(contentType, accept, os, version, timezone, deviceId, token)) {
                logger.info("SeqId = " + seqId + "| Bad header");
                return ResponseUtils.BadRequest(language, seqId, ErrorCode.BAD_REQUEST_HEADER, request);
            }

            // check null or empty request body
            if (Utils.isNullOrEmpty(body)) {
                logger.info("SeqId = " + seqId + "| Null body");
                return ResponseUtils.BadRequest(language, seqId, ErrorCode.NULL_REQUEST_BODY, request);
            }

            // parse request body json
            JsonObject bodyObj;
            try {
                bodyObj = new Gson().fromJson(body, JsonObject.class);
            } catch (JsonSyntaxException je) {
                logger.info("SeqId = " + seqId + "| Bad body json");
                return ResponseUtils.BadRequest(language, seqId, ErrorCode.BAD_BODY_JSON, request);

            }

            // check required field
            JsonElement phoneOrEmailE = bodyObj.get("phoneOrEmail");
            JsonElement passwordE = bodyObj.get("password");

            if (Utils.isNull(phoneOrEmailE)) {
                logger.info("SeqId = " + seqId + "| phoneOrEmail is null!");
                return ResponseUtils.BadRequest(language, seqId, ErrorCode.BAD_REQUEST, request);
            }

            if (Utils.isNull(passwordE)) {
                logger.info("SeqId = " + seqId + "| password is null!");
                return ResponseUtils.BadRequest(language, seqId, ErrorCode.BAD_REQUEST, request);
            }

            String username = Token.getUsernameFromToken(token, seqId);
            String phoneOrEmail = phoneOrEmailE.getAsString();
            String password = passwordE.getAsString();

            if (Utils.isNullOrEmpty(username)) {
                logger.info("SeqId = " + seqId + "| username is empty! Bad token");
                return ResponseUtils.BadRequest(language, seqId, ErrorCode.BAD_REQUEST, request);
            }

            if (Utils.isEmpty(phoneOrEmail) || phoneOrEmail.trim().length() < 2) {
                logger.info("SeqId = " + seqId + "| phoneOrEmail is empty or < 2 chars!");
                return ResponseUtils.BadRequest(language, seqId, ErrorCode.BAD_REQUEST, request);
            }

            if (Utils.isEmpty(password) || password.trim().length() < 2) {
                logger.info("SeqId = " + seqId + "| password is empty or < 2 chars!");
                return ResponseUtils.BadRequest(language, seqId, ErrorCode.BAD_REQUEST, request);
            }

            // prepare to call Core service
            JsonObject req = new JsonObject();
            req.addProperty("username", username);
            req.addProperty("password", password);
            req.addProperty("phoneOrEmail", phoneOrEmail);
            req.addProperty("languageId", language);

            CoreCommonV1 coreCommonV1 = new CoreCommonV1();
            resApi = WsUtils.readResult(coreCommonV1.postWithPath(seqId, req, corePath));

            JsonObject joResFromCore = new Gson().fromJson(resApi.getEntity().toString(), JsonObject.class);
            logger.info("SeqId = " + seqId + "| Response from Core service = " + joResFromCore.toString().replaceAll("\r\n", ""));

            if (Utils.checkResponseFromCore(joResFromCore)) {


            } else {
                return ResponseUtils.CoreServiceProblem(joResFromCore, seqId, request);

            }

            // build response
            JsonObject jo1 = new Gson().fromJson(resApi.getEntity().toString(), JsonObject.class);
            if (Utils.checkResponseFromCore(jo1)) {
                JsonObject jo2 = new JsonObject();
                jo2.add("username", jo1.get("username"));
                jo2.add("otpExpiredTime", jo1.get("otpExpiredTime"));
                JsonObject jo3 = new JsonObject();
                jo3.add("data", jo2);
                logger.error("SeqId = "+seqId + "| Exception = "  + jo3.toString());
                Response response = ResponseUtils.OK(language, jo3, dataModel, resApi);
                logger.info("SeqId = " + seqId + "| Response for Request = " + request.getRequestURI() + "| Response = " + response.getEntity().toString());
                return response;
            } else {
                return ResponseUtils.CoreServiceProblem(jo1, seqId, request);
            }

        } catch (Exception exception) {
            logger.error("SeqId = "+seqId + "| Exception = " , exception);
            exception.printStackTrace();
            Response response = ResponseUtils.Exception(language);
            logger.info("SeqId = " + seqId + "| Response for Request = " + request.getRequestURI() + "| Response = " + response.getEntity().toString());
            return response;
        } finally {
            CounterRequest.sessionDestroy();
        }

    }
}
