package com.mfs.modoapi.api;

import com.google.gson.JsonObject;
import com.mfs.modoapi.util.CounterRequest;
import com.mfs.modoapi.util.GeneratorSeq;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.util.List;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;;

@Path("v1")
public class PingMultipartApi {

    private static final Logger logger = Logger.getLogger(PingMultipartApi.class);

    @POST
    @Path("ping-multipart")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response pingApi(@Context HttpServletRequest request) {
        try {

            if (ServletFileUpload.isMultipartContent(request)) {
                DiskFileItemFactory factory = new DiskFileItemFactory();
//                factory.setSizeThreshold(MEMORY_THRESHOLD);
                factory.setRepository(new File("E:\\chao.txt"));
                ServletFileUpload upload = new ServletFileUpload(factory);
                List<FileItem> formItems = upload.parseRequest(request);
                if (formItems != null && formItems.size() > 0) {
                    for (FileItem item : formItems) {
                        if (!item.isFormField()) {
                            String fileName = new File(item.getName()).getName();
                            logger.info( "| file file  size = " + item.getSize());

                            ByteArrayOutputStream baos = new ByteArrayOutputStream();
                            byte[] buf = new byte[1024];
                            int n = 0;
                            InputStream myInputStream = item.getInputStream();

                            while ((n = myInputStream.read(buf)) != -1){
                                logger.info("read: " + n);
                                baos.write(buf, 0, n);


                            }
                            baos.flush();
                            byte[] content = baos.toByteArray();

                            logger.info( "| arr size = " + content.length);

                            logger.info("file name: " + fileName);
                            logger.info("file getFieldName: " + item.getFieldName());
                        } else {
                            logger.info(item.getFieldName());
                        }
                    }
                }

            }
            CounterRequest.sessionCreated();
            int seqId = GeneratorSeq.getNextSeq();
            logger.info("SeqId: " + seqId + "| Request: " + request.getRequestURI());
            logger.info("SeqId: " + seqId + "| chao: " + request.getParameter("chao"));
//            logger.info("SeqId: " + seqId + "| file name: " + fileDetail.getName());



            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("result", "thành cổng nhé ---");


            Response response = Response.status(200).entity(jsonObject.toString()).build();
            logger.info(response.toString());
            return response;
        } catch (Exception exception) {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("message", "Sorry-Server Error");
            return Response.status(500).entity(jsonObject.toString()).build();
        } finally {
            CounterRequest.sessionDestroy();
        }

    }
}
