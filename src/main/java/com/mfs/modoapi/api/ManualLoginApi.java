package com.mfs.modoapi.api;


import com.mfs.modoapi.util.CounterRequest;
import com.mfs.modoapi.util.GeneratorSeq;
import com.mfs.modoapi.util.ResbundleResourceManager;
import com.mfs.modoapi.util.Utils;
import com.mfs.modoapi.ws.CoreLoginWS;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("manualLogin")
public class ManualLoginApi {

    private static final Logger logger = Logger.getLogger(ManualLoginApi.class);


    @POST
    @Path("phoneNumber")
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response postIsdn(@Context HttpServletRequest request,
                             @FormParam("MSISDN") String msisdn,
                             @HeaderParam("deviceId") String deviceId,
                             @FormParam("osType")String osType,
                             @FormParam("osVersion") String osVersion,
                             @FormParam("appLanguage") String appLanguage

                             ) {
        CounterRequest.sessionCreated();
        long seqId = GeneratorSeq.getNextSeq();
        Response resApi = null;
        try {
            logger.info("seqid=" + seqId + " request: " + request.getRequestURI());
            if (Utils.isNullOrEmpty(msisdn, deviceId)) {
                resApi = Utils.badRequest();
                logger.info("SeqId=" + seqId + "| Response: " + resApi.toString());
                return resApi;
            }

            CoreLoginWS coreLoginWS = new CoreLoginWS();
             resApi = coreLoginWS.putSentIsdn(ResbundleResourceManager.getUserCoreWs(),
                    ResbundleResourceManager.getPassCoreWs(), msisdn, osType, osVersion, appLanguage, deviceId,seqId);
             logger.info("Status: " + resApi.getStatus());

             // TODO tunv kinh nghiem kinh nghiệm
             // https://stackoverflow.com/questions/36946178/illegalstateexception-within-method-with-response-paramether
            logger.info("seqid=" + seqId + " response: " + resApi.getEntity()); // resApi.readEntity(String.class) chỉ hoạt động khi gọi api


            return resApi;



        } catch (Exception ex) {
            resApi = Utils.internalServerError();
            logger.error("x22", ex);
            logger.info("SeqId=" + seqId + "| Response: " + resApi.toString());
            return resApi;
        } finally {
            CounterRequest.sessionDestroy();
        }


    }
}
