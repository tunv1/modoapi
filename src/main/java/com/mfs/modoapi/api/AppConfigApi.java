package com.mfs.modoapi.api;

import com.google.gson.JsonObject;
import com.mfs.modoapi.util.GeneratorSeq;
import com.mfs.modoapi.util.CounterRequest;
import com.mfs.modoapi.util.Utils;
import com.mfs.modoapi.ws.CoreAppConfigWS;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("appconfig")
public class AppConfigApi {
    private static final Logger logger = Logger.getLogger(AppConfigApi.class);

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAppConfig(@Context HttpServletRequest request,
                                 @QueryParam("appVersion") String appVersion,
                                 @QueryParam("language") String language,
                                 @QueryParam("phoneNumber") String phoneNumber) {
        Response resApi;
        CounterRequest.sessionCreated();
        int seqId = GeneratorSeq.getNextSeq();
        logger.info("SeqId=" + seqId + "| Request: " + request.getRequestURI());
        JsonObject jo = new JsonObject();
        try {

            if (Utils.isNullOrEmpty(appVersion, language, phoneNumber)) {
                resApi = Utils.badRequest();
                logger.info("SeqId=" + seqId + "| Response: " + resApi.toString());
                return resApi;
            }

            CoreAppConfigWS coreAppConfigWS = new CoreAppConfigWS();



            jo.addProperty("message", "OK");
            resApi = Utils.ok(jo);
            logger.info("SeqId=" + seqId + "| Response: " + resApi.toString());
            return resApi;
        } catch (Exception ex) {
            resApi = Utils.internalServerError();
            logger.info("SeqId=" + seqId + "| Response: " + resApi.toString());
            return resApi;
        } finally {
            CounterRequest.sessionDestroy();
        }
    }
}
