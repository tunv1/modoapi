package com.mfs.modoapi.api.doctor.v1;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.mfs.modoapi.util.*;
import com.mfs.modoapi.ws.doctor.v1.CoreLoginDoctorV1;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("doctor/v1")
public class LoginDoctorApiV1 {

    private static final Logger logger = Logger.getLogger(LoginDoctorApiV1.class);

    @POST
    @Path("login")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response loginDoctor(@Context HttpServletRequest request,

                           @HeaderParam("OS") String os

            , String body, @QueryParam("type") String type) {

        CounterRequest.sessionCreated();
        int seqId = GeneratorSeq.getNextSeq();
        Response resApi = null;
        CoreLoginDoctorV1 coreLoginDoctorV1 = new CoreLoginDoctorV1();
        int status = 200;

        try {

            logger.info("SeqId: " + seqId + "| Request: " + request.getRequestURI());

            logger.info("Body: " + body);
            JsonObject requestBodyObj = new Gson().fromJson(body, JsonObject.class);
            logger.info("type = " + type);
            if (type != null && type.length() > 0) {
                switch (type) {
                    case LoginType.FACEBOOK:
                        logger.info("login fia facebook");
                        logger.info("accessToken = " + requestBodyObj.get("accessToken"));
                        break;
                    case LoginType.GOOGLE:
                        logger.info("login via google");
                        logger.info("accessToken = " + requestBodyObj.get("accessToken"));
                        break;
                    default:
                        break;
                }
            } else {
                logger.info("login via phone number");
                logger.info("phone: " + requestBodyObj.get("phone"));
                logger.info("password: " + requestBodyObj.get("password"));

                resApi = WsUtils.readResult(coreLoginDoctorV1.postLoginPhone(seqId, requestBodyObj.get("phone").getAsString(),
                        requestBodyObj.get("password").getAsString()));

                logger.info("resApi = " + resApi.getEntity().toString());

            }

            JsonObject jsonObject = new JsonObject();

            JsonObject err = new JsonObject();
            JsonObject jo1 = new Gson().fromJson(resApi.getEntity().toString(), JsonObject.class);
            if (Utils.checkResponseFromCore(jo1)) {
                jsonObject.add("data", jo1.get("doctorInfo").getAsJsonObject());
                err.addProperty("code", "E0000");
                err.addProperty("message", "OK");
                jsonObject.addProperty("status", true);
                jsonObject.add("error", null);
            } else {
                jsonObject.addProperty("status", false);
                err.addProperty("code", "E0002");
                err.addProperty("message", jo1.get("rstext").getAsString());
                jsonObject.add("error", err);
                status = 400;
            }

            Response response = Response.status(200).entity(jsonObject.toString()).build();
            logger.info("response: " + response.getEntity().toString());
            return response;
        } catch (Exception exception) {
            logger.error("seqid= "+seqId + "__", exception);
            exception.printStackTrace();
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("message", "Sorry-Server Error");
            return Response.status(500).entity(jsonObject.toString()).build();
        } finally {
            CounterRequest.sessionDestroy();
        }

    }
}
