package com.mfs.modoapi.api.doctor.v1;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.mfs.modoapi.util.*;
import com.mfs.modoapi.ws.doctor.v1.CoreSignUpDoctorV1;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("doctor/v1")
public class SignUpDoctorApiV1 {

    private static final Logger logger = Logger.getLogger(SignUpDoctorApiV1.class);

    @POST
    @Path("sign-up")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response signUp(@Context HttpServletRequest request,
                           @HeaderParam("Content-Type") String contentType,
                           @HeaderParam("Accept") String accept,
                           @HeaderParam("OS") String os,
                           @HeaderParam("Version") String version,
//                           @HeaderParam("Token") String token,
                           @HeaderParam("Language") String language,
                           @HeaderParam("Timezone") String timezone,
                           @HeaderParam("DeviceId") String deviceId,
                           String body) {
        CounterRequest.sessionCreated();
        int seqId = GeneratorSeq.getNextSeq();
        Response resApi = null;
        boolean responseAppStatus = true;
        int responseStatus = 200;
        String responseMess = "OK";
        String responseCode = "E0000";
        try {

            logger.info("SeqId=" + seqId + "|Accept Request=" + request.getRequestURI());
//            logger.info("SeqId: " + seqId
//                    + "| Content-Type: " + contentType
//                    + "| Accept: " + accept
//                    + "| OS: " + os
//                    + "| Version: " + version
////                    + "| Token: " + token
//                    + "| Language: " + language
//                    + "| Timezone: " + timezone
//                    + "| DeviceId: " + deviceId
//
//            );
//
//            logger.info("SeqId: " + seqId + "| Body: " + body);

            JsonObject bodyObj = new Gson().fromJson(body, JsonObject.class);
//            logger.info("SeqId: " + seqId
//                    + "| Email: " + bodyObj.get("email")
//                    + "| firstname: " + bodyObj.get("firstname")
//                    + "| lastname: " + bodyObj.get("lastname")
//                    + "| password: " + bodyObj.get("password")
//            );


            JsonObject req = new JsonObject();
            req.add("phone", bodyObj.get("phone"));
            req.add("firstname", bodyObj.get("firstname"));
            req.add("lastname", bodyObj.get("lastname"));
            req.add("password", bodyObj.get("password"));
            req.add("gender", bodyObj.get("gender"));
            req.add("birthday", bodyObj.get("birthday"));
            req.add("email", bodyObj.get("email"));
            req.add("departmentId", bodyObj.get("departmentId"));
            req.add("degreeId", bodyObj.get("degreeId"));
            req.add("graduateYear", bodyObj.get("graduateYear"));
            req.add("provinceId", bodyObj.get("provinceId"));
            req.add("address", bodyObj.get("address"));
            req.add("infomation", bodyObj.get("infomation"));
            req.add("avatarUrl", bodyObj.get("avatarUrl"));
            req.add("degreeUrl", bodyObj.get("degreeUrl"));
            req.addProperty("os", os);
            req.addProperty("version", version);
            req.addProperty("language", language);
            req.addProperty("timezone", timezone);
            req.addProperty("deviceId", deviceId);

            CoreSignUpDoctorV1 coreSignUpDoctorV1 = new CoreSignUpDoctorV1();
            Response res = coreSignUpDoctorV1.getUsernameSignUp(seqId, req);
            resApi = WsUtils.readResult(res);

            JsonObject joResFromCoreUsername = new Gson().fromJson(resApi.getEntity().toString(), JsonObject.class);

            logger.info("joResFromCoreUsername" + joResFromCoreUsername.toString());

            if (Utils.checkResponseFromCore(joResFromCoreUsername)) {
                String username = joResFromCoreUsername.get("username").getAsString();
                String tokenStringee = Token.genAccessToken(Constants.API_KEY_STRINGEE, Constants.SECRET_KEY_STRINGEE, 10000000, username);

                String tokenMFS = Token.genAccessToken(Constants.API_KEY_MFS, Constants.SECRET_KEY_MFS, 10000000, username);

                // send token to core to save to DB

                Response responsePostUpdateToken = coreSignUpDoctorV1.postUpdateToken(seqId, username, tokenMFS, tokenStringee);
                resApi = WsUtils.readResult(responsePostUpdateToken);
                logger.info(resApi.getEntity().toString());

                JsonObject joRes = new Gson().fromJson(resApi.getEntity().toString(), JsonObject.class);
                if (Utils.checkResponseFromCore(joRes)) {

                } else {
                    responseAppStatus = false;
                    responseCode = "E0004";
                    logger.error("SeqId=" + seqId + "|Err when update doctor info" );
                    responseMess = joRes.get("rstext").getAsString();
                    responseStatus = 500;
                }

            } else {
                responseAppStatus = false;
                responseCode = "E0003";
                responseMess = joResFromCoreUsername.get("rstext").getAsString();
                responseStatus = 500;
            }

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("status", responseAppStatus);
            JsonObject err = new JsonObject();
            err.addProperty("code", responseCode);
            err.addProperty("message", responseMess);
            jsonObject.add("error", err);

            if (responseStatus == 200) {
                JsonObject jo1 = new Gson().fromJson(resApi.getEntity().toString(), JsonObject.class);
                jsonObject.add("data", jo1.get("doctorInfo").getAsJsonObject());
            }

            Response response = Response.status(responseStatus).entity(jsonObject.toString()).build();
            logger.info("SeqId=" + seqId + "|Response for Request=" + request.getRequestURI() + "|Response=" + response.getEntity().toString());
            return response;
        } catch (Exception exception) {
            logger.error("seqid= "+seqId + "__", exception);
            exception.printStackTrace();
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("message", "Sorry-Server Error");
            return Response.status(200).entity(jsonObject.toString()).build();
        } finally {
            CounterRequest.sessionDestroy();
        }

    }
}
