package com.mfs.modoapi.api.doctor.v1;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.mfs.modoapi.util.CounterRequest;
import com.mfs.modoapi.util.GeneratorSeq;
import com.mfs.modoapi.util.Utils;
import com.mfs.modoapi.util.WsUtils;
import com.mfs.modoapi.ws.doctor.v1.CoreGetDoctorDetailV1;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("doctor/v1")
public class GetDoctorDetailApiV1 {

    private static final Logger logger = Logger.getLogger(GetDoctorDetailApiV1.class);

    @GET
    @Path("getDoctorDetail")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getDoctors(@Context HttpServletRequest request,
                               @HeaderParam("language") String language ,
                               @QueryParam("username") String username

    ) {

        CounterRequest.sessionCreated();
        int seqId = GeneratorSeq.getNextSeq();
        Response resApi = null;
        int status = 200;

        try {
            logger.info("SeqId=" + seqId + "|Accept Request=" + request.getRequestURI());

            JsonObject jsonObject = new JsonObject();
            JsonObject err = new JsonObject();
            err.addProperty("code", "E0000");
            err.addProperty("message", "OK");

            int lang;
            if (language.equalsIgnoreCase("en")) {
                lang = 1;
            } else {
                lang = 0;
            }

            CoreGetDoctorDetailV1 coreGetDoctorDetailV1 = new CoreGetDoctorDetailV1();
            Response res = coreGetDoctorDetailV1.getDoctorDetail(seqId, lang, username);
            resApi = WsUtils.readResult(res);

            JsonObject jo1 = new Gson().fromJson(resApi.getEntity().toString(), JsonObject.class);

            if (Utils.checkResponseFromCore(jo1)) {
                jsonObject.add("data", jo1.get("doctorInfo"));
                err.addProperty("code", "E0000");
                err.addProperty("message", "OK");
                jsonObject.addProperty("status", true);
                jsonObject.add("error", null);
            } else {
                jsonObject.addProperty("status", false);
                err.addProperty("code", "E0004");
                err.addProperty("message", jo1.get("rstext").getAsString());
                jsonObject.add("error", err);
                status = 400;
            }

            logger.info(resApi.getEntity().toString());



            Response response = Response.status(200).entity(jsonObject.toString()).build();
            logger.info("SeqId=" + seqId + "|Response for Request=" + request.getRequestURI() + "|Response=" + response.getEntity().toString());

            return response;
        } catch (Exception exception) {
            logger.error("seqid= "+seqId + "__", exception);
            exception.printStackTrace();
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("message", "Sorry-Server Error");
            return Response.status(500).entity(jsonObject.toString()).build();
        } finally {
            CounterRequest.sessionDestroy();
        }

    }
}
