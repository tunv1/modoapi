package com.mfs.modoapi.api.doctor.v1;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.mfs.modoapi.util.CounterRequest;
import com.mfs.modoapi.util.GeneratorSeq;
import com.mfs.modoapi.util.Utils;
import com.mfs.modoapi.util.WsUtils;
import com.mfs.modoapi.ws.doctor.v1.CoreOtpDoctorV1;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("doctor/v1")
public class OtpDoctorApiV1 {

    private static final Logger logger = Logger.getLogger(OtpDoctorApiV1.class);

    @POST
    @Path("otp")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response login(@Context HttpServletRequest request,

                           @HeaderParam("OS") String os

            , String body, @QueryParam("type") String type) {

        CounterRequest.sessionCreated();
        int seqId = GeneratorSeq.getNextSeq();
        Response resApi = null;
        boolean responseAppStatus = true;
        int responseStatus = 200;
        String responseMess = "OK";
        String responseCode = "E0000";

        try {

            logger.info("SeqId: " + seqId + "| Request: " + request.getRequestURI());

            JsonObject requestBodyObj = new Gson().fromJson(body, JsonObject.class);

            logger.info("SeqId: " + seqId + "| Json body: " + requestBodyObj);

            if (requestBodyObj.get("username").getAsString() != null &&
                    requestBodyObj.get("username").getAsString().length() > 0 &&
                    requestBodyObj.get("otp").getAsString() != null &&
                    requestBodyObj.get("otp").getAsString().length() > 0 ) {


                JsonObject jo = new JsonObject();
                jo.addProperty("username", requestBodyObj.get("username").getAsString());
                jo.addProperty("otp", requestBodyObj.get("otp").getAsString());
                CoreOtpDoctorV1 coreOtpV1 = new CoreOtpDoctorV1();
                Response res = coreOtpV1.postOtp(seqId, jo.toString());
                resApi = WsUtils.readResult(res);

                logger.info("SeqId: " + seqId + "| Response from Core: " + resApi.getEntity().toString());
                JsonObject coreResponse = new Gson().fromJson(resApi.getEntity().toString(), JsonObject.class);
                if (Utils.checkResponseFromCore(coreResponse)) {


                } else {
                    responseAppStatus = false;
                    responseCode = "E0007";
                    responseMess = coreResponse.get("rstext").getAsString();
                    responseStatus = 400;
                }

            } else {
                responseAppStatus = false;
                responseCode = "E0004";
                responseMess = "Bad request";
                responseStatus = 701;
            }

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("status", responseAppStatus);
            JsonObject err = new JsonObject();
            err.addProperty("code", responseCode);
            err.addProperty("message", responseMess);
            jsonObject.add("error", err);

            if (responseStatus == 200) {
                JsonObject jo1 = new Gson().fromJson(resApi.getEntity().toString(), JsonObject.class);
                jsonObject.add("data", jo1.get("doctorInfo").getAsJsonObject());
            }

            Response response = Response.status(200).entity(jsonObject.toString()).build();
            logger.info("SeqId=" + seqId + "|Response for Request=" + request.getRequestURI() + "|Response=" + response.getEntity().toString());
            return response;
        } catch (Exception exception) {
            logger.error("seqid= "+seqId + "__", exception);
            exception.printStackTrace();
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("message", "Sorry-Server Error");
            return Response.status(500).entity(jsonObject.toString()).build();
        } finally {
            CounterRequest.sessionDestroy();
        }

    }
}
