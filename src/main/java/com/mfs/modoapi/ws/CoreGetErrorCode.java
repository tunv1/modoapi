package com.mfs.modoapi.ws;

import com.google.gson.JsonObject;
import com.mfs.modoapi.util.ResbundleResourceManager;
import org.apache.log4j.Logger;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientProperties;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

public class CoreGetErrorCode {

    private WebTarget webTarget;
    private Client client;

    private static final Logger logger = Logger.getLogger(CoreGetErrorCode.class);

    public CoreGetErrorCode() {
        ClientConfig configuration = new ClientConfig();
        configuration.property(ClientProperties.CONNECT_TIMEOUT, ResbundleResourceManager.getTimeoutConnectCoreWs());
        configuration.property(ClientProperties.READ_TIMEOUT, ResbundleResourceManager.getTimeoutReadCoreWs());
        configuration.property(ClientProperties.ASYNC_THREADPOOL_SIZE,10);
        client = ClientBuilder.newClient(configuration);
        webTarget =client.target(ResbundleResourceManager.getUriCore());

    }

    public Response getErrorCode(int seqId) throws Exception {
        WebTarget webTarget = this.webTarget.path("getErrorCode") ;
        logger.info("SeqId = " + seqId + "| Start call API " + webTarget.getUri());
        Response response = webTarget.request(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(Response.class);
        logger.info("SeqId = " + seqId + "| End call API " + webTarget.getUri() + "| Response = " + response.toString());

        return response;

    }


}
