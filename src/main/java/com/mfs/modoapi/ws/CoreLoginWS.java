package com.mfs.modoapi.ws;

import com.google.gson.JsonObject;
import com.mfs.modoapi.util.ResbundleResourceManager;
import com.mfs.modoapi.util.StringUtils;
import org.apache.log4j.Logger;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientProperties;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

public class CoreLoginWS {

    private WebTarget webTarget;
    private Client client;

    private static final Logger logger = Logger.getLogger(CoreLoginWS.class);

    public CoreLoginWS() {
        ClientConfig configuration = new ClientConfig();
        configuration.property(ClientProperties.CONNECT_TIMEOUT, ResbundleResourceManager.getTimeoutConnectCoreWs());
        configuration.property(ClientProperties.READ_TIMEOUT, ResbundleResourceManager.getTimeoutReadCoreWs());
        configuration.property(ClientProperties.ASYNC_THREADPOOL_SIZE,10);
        client = ClientBuilder.newClient(configuration);
        webTarget =client.target(ResbundleResourceManager.getUriCore());

    }

    public Response putSentIsdn(String user, String pass, String isdn,
                                String osType,
                                String osVersion,
                                String appLanguage,
                                String deviceId , long seqId
    ) throws ClientErrorException {

        WebTarget webTarget = this.webTarget.path("login/checkOtp")
                .queryParam("username", StringUtils.UrlEncode(user))
                .queryParam("password", StringUtils.UrlEncode(pass))
                .queryParam("msisdn", StringUtils.UrlEncode(isdn))
                .queryParam("osType", StringUtils.UrlEncode(osType))
                .queryParam("osVersion", StringUtils.UrlEncode(osVersion))
                .queryParam("appLanguage", StringUtils.UrlEncode(appLanguage))
                .queryParam("deviceId", StringUtils.UrlEncode(deviceId))
                .queryParam("seqId", StringUtils.UrlEncode(String.valueOf(seqId)))
                ;

        logger.info("seqid=" + seqId + "  request: " + webTarget.getUri());
//        Response res = webTarget.request().put(Entity.json(""), Response.class);
        JsonObject jo = new JsonObject();
        jo.addProperty("isSuccess", true);
        Response res =  Response.status(Response.Status.OK).entity(jo.toString()).build();
        logger.info("seqid=" + seqId + "  response: " + res.toString());

        return res;

    }

}
