package com.mfs.modoapi.ws.patient.v1;

import com.google.gson.JsonObject;
import com.mfs.modoapi.util.ResbundleResourceManager;
import org.apache.log4j.Logger;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientProperties;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

public class CoreOtpV1 {

    private WebTarget webTarget;
    private Client client;

    private static final Logger logger = Logger.getLogger(CoreOtpV1.class);

    public CoreOtpV1() {
        ClientConfig configuration = new ClientConfig();
        configuration.property(ClientProperties.CONNECT_TIMEOUT, ResbundleResourceManager.getTimeoutConnectCoreWs());
        configuration.property(ClientProperties.READ_TIMEOUT, ResbundleResourceManager.getTimeoutReadCoreWs());
        configuration.property(ClientProperties.ASYNC_THREADPOOL_SIZE,10);
        client = ClientBuilder.newClient(configuration);
        webTarget =client.target(ResbundleResourceManager.getUriCore());

    }

    public Response postOtp(int seqId, JsonObject jsonObject ) throws Exception {

        WebTarget webTarget = this.webTarget.path("otp") ;
        logger.info("SeqId = " + seqId + "| Start call API " + webTarget.getUri() + "| Request = " + jsonObject.toString());
        Response response = webTarget.request().post(Entity.json(jsonObject.toString()), Response.class);
        logger.info("SeqId = " + seqId + "| End call API " + webTarget.getUri() + "| Response = " + response.toString());

        return response;

    }


}
