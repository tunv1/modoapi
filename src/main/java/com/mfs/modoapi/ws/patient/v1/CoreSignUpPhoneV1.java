package com.mfs.modoapi.ws.patient.v1;

import com.google.gson.JsonObject;
import com.mfs.modoapi.util.ResbundleResourceManager;
import com.mfs.modoapi.util.Utils;
import org.apache.log4j.Logger;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientProperties;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

public class CoreSignUpPhoneV1 {

    private WebTarget webTarget;
    private Client client;

    private static final Logger logger = Logger.getLogger(CoreSignUpPhoneV1.class);

    public CoreSignUpPhoneV1() {
        ClientConfig configuration = new ClientConfig();
        configuration.property(ClientProperties.CONNECT_TIMEOUT, ResbundleResourceManager.getTimeoutConnectCoreWs());
        configuration.property(ClientProperties.READ_TIMEOUT, ResbundleResourceManager.getTimeoutReadCoreWs());
        configuration.property(ClientProperties.ASYNC_THREADPOOL_SIZE,10);
        client = ClientBuilder.newClient(configuration);
        webTarget =client.target(ResbundleResourceManager.getUriCore());

    }

    public Response postForUsername(int seqId, JsonObject jo1 ) {

        WebTarget webTarget = this.webTarget.path("signup-phone");

        logger.info("SeqId = " + seqId + "| Start call API " + webTarget.getUri() + "| Request = " + jo1.toString());
        Response response = webTarget.request().post(Entity.json(jo1.toString()), Response.class);
        logger.info("SeqId = " + seqId + "| End call API " + webTarget.getUri() + "| Response = " + response.toString());

        return response;

    }

    public Response  postUpdateToken(int seqId, String username, String tokenMFS, String tokenStringee, int langguage ) throws Exception {

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("username", username);
        jsonObject.addProperty("token", tokenMFS);
        jsonObject.addProperty("callToken", tokenStringee);
        jsonObject.addProperty("languageId", langguage);
        WebTarget webTarget = this.webTarget.path("user-update");

        logger.info("SeqId = " + seqId + "| Start call API " + webTarget.getUri() + "| Request = " + jsonObject.toString());
        Response response = webTarget.request().post(Entity.json(jsonObject.toString()), Response.class);
        logger.info("SeqId = " + seqId + "| End call API " + webTarget.getUri() + "| Response = " + response.toString());

        return response;

    }

}
