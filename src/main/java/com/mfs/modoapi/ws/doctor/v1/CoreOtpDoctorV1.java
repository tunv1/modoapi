package com.mfs.modoapi.ws.doctor.v1;

import com.mfs.modoapi.util.ResbundleResourceManager;
import org.apache.log4j.Logger;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientProperties;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

public class CoreOtpDoctorV1 {

    private WebTarget webTarget;
    private Client client;

    private static final Logger logger = Logger.getLogger(CoreOtpDoctorV1.class);

    public CoreOtpDoctorV1() {
        ClientConfig configuration = new ClientConfig();
        configuration.property(ClientProperties.CONNECT_TIMEOUT, ResbundleResourceManager.getTimeoutConnectCoreWs());
        configuration.property(ClientProperties.READ_TIMEOUT, ResbundleResourceManager.getTimeoutReadCoreWs());
        configuration.property(ClientProperties.ASYNC_THREADPOOL_SIZE,10);
        client = ClientBuilder.newClient(configuration);
        webTarget =client.target(ResbundleResourceManager.getUriCore());

    }

    public Response postOtp(int seqId, String jsonObject
    ) throws ClientErrorException {

        WebTarget webTarget = this.webTarget.path("doctor/otp")
                ;
        logger.info("SeqId=" + seqId + "|start call= " + webTarget.getUri());

        Response response = webTarget.request().post(Entity.json(jsonObject.toString()), Response.class);

        logger.info("SeqId=" + seqId + "|end call= " + webTarget.getUri() + "|response=" + response.toString());

        return response;

    }


}
