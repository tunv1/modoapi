package com.mfs.modoapi.ws.doctor.v1;

import com.google.gson.JsonObject;
import com.mfs.modoapi.util.ResbundleResourceManager;
import org.apache.log4j.Logger;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientProperties;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

public class CoreSignUpDoctorV1 {

    private WebTarget webTarget;
    private Client client;

    private static final Logger logger = Logger.getLogger(CoreSignUpDoctorV1.class);

    public CoreSignUpDoctorV1() {
        ClientConfig configuration = new ClientConfig();
        configuration.property(ClientProperties.CONNECT_TIMEOUT, ResbundleResourceManager.getTimeoutConnectCoreWs());
        configuration.property(ClientProperties.READ_TIMEOUT, ResbundleResourceManager.getTimeoutReadCoreWs());
        configuration.property(ClientProperties.ASYNC_THREADPOOL_SIZE,10);
        client = ClientBuilder.newClient(configuration);
        webTarget =client.target(ResbundleResourceManager.getUriCore());

    }

    public Response getUsernameSignUp(int seqId, JsonObject jo1
    ) throws ClientErrorException {

        WebTarget webTarget = this.webTarget.path("doctor/sign-up");

        logger.info("SeqId=" + seqId + "|start call= " + webTarget.getUri() + "|request=" + jo1.toString());
        Response response = webTarget.request().post(Entity.json(jo1.toString()), Response.class);
        logger.info("SeqId=" + seqId + "|end call= " + webTarget.getUri() + "|response=" + response.toString());

        return response;

    }

    public Response  postUpdateToken(int seqId, String username, String tokenMFS, String tokenStringee
    ) throws ClientErrorException {

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("username", username);
        jsonObject.addProperty("token", tokenMFS);
        jsonObject.addProperty("callToken", tokenStringee);
        WebTarget webTarget = this.webTarget.path("doctor/user-update");

        logger.info("SeqId=" + seqId + "|start call= " + webTarget.getUri() + "|request=" + jsonObject.toString());
        Response response = webTarget.request().post(Entity.json(jsonObject.toString()), Response.class);
        logger.info("SeqId=" + seqId + "|end call= " + webTarget.getUri() + "|response=" + response.toString());

        return response;

    }

}
