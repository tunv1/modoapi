/*
* Copyright (C) 2016 Mobifone Service. All rights reserved.
* MOBIFONESERVICE/VAS. Use is subject to license terms.
 */
package com.mfs.modoapi.util;

import javax.crypto.spec.SecretKeySpec;
import java.security.Key;

/**
 *
 * @author phannkvos
 */
public final class Constants {

//time out for token, unit by millis secon
    public static final long TIMEOUT_TOKEN = -1;
//    public static final long TIMEOUT_TOKEN = 7200000;
    public static final long TIMEOUT_CORE_WS = -1;
    public static final int TIMEOUT_CONNECT_CORE_WS = 1000;
    public static final int TIMEOUT_READ_CORE_WS = 1000;
    // Key ramdom for token
//    public static final Key key = MacProvider.generateKey();
    //sinh key cứng
    public static final Key key =  new SecretKeySpec("next@2016".getBytes(),  "AES");

    public static final int RESPONSE_SUCC = 200;
    public static final int RESPONSE_INVA_PARAM = 400;
    public static final int RESPONSE_UNAUTH = 401;
    public static final int RESPONSE_ERROR = 500;
    public static final int RESPONSE_OVERLOAD = 429;
    //
    public static final String PROJECT_CODE = "SMARTTOPUP";
    public static final String ERROR_CODE = "SMARTTOPUP";
    public static final String ERROR_SEVERITY = "ERROR";


    public static final String API_KEY_STRINGEE = "SKVdXedZP6Sp6KPNW8E6i0u5I23VB9WvjG";
    public static final String SECRET_KEY_STRINGEE = "TXFQSGFhZjZuVlVBQ004V0h1UVlUSnFRUjJYY0Vq";


    public static final String API_KEY_MFS = "SKvQ4xJDhXmYwg1fvMFDINTHEWORLDDPi9AGdSffKUMH7MFDINTHEWORLD";
    public static final String SECRET_KEY_MFS = "RDQ4RkJ1U1NhbTJUbGN5TEdFMzJPNUpkZFduYVNZTQ==MFDINTHEWORLD";


    // Seconds
    public static final int EXPIRED_TOKEN_STRINGEE = 1000000;

    public static final int EXPIRED_TOKEN_MFS = 315360000; // 10 years

    public static String GOOGLE_LINK_GET_USER_INFO = "https://www.googleapis.com/oauth2/v1/userinfo?access_token=";
    public static String FACEBOOK_LINK_GET_USER_INFO = "https://graph.facebook.com/me?access_token=";


    // Tu Nguyen
//    public static String GOOGLE_CLIENT_ID = "45263658763-9ibfn75ie12u9l13oo1use8vrenh1m03.apps.googleusercontent.com";
//    public static String GOOGLE_CLIENT_SECRET = "pCwT-_ZzODWByxU_bhyYlAF1";
//    public static String GOOGLE_REDIRECT_URI = "https://localhost:8443/shop/GoogleLoginRedirect";
//    public static String GOOGLE_LINK_GET_TOKEN = "https://accounts.google.com/o/oauth2/token";
//    public static String GOOGLE_GRANT_TYPE = "authorization_code";

    // MFS
    // Chú ý phía app cũng phải cấu hình tương tự khi gọi api để lấy auth_code (code)
    public static String GOOGLE_CLIENT_ID = "404003840965-fvopr3p6lqi6s3qc8lupgbefg94ot4mh.apps.googleusercontent.com";
    public static String GOOGLE_CLIENT_SECRET = "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";
    public static String GOOGLE_REDIRECT_URI = "https://modo-14ae3.firebaseapp.com/__/auth/handler";
    public static String GOOGLE_LINK_GET_TOKEN = "https://accounts.google.com/o/oauth2/token";
    public static String GOOGLE_GRANT_TYPE = "authorization_code";

}
