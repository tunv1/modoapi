package com.mfs.modoapi.util;

import com.mfs.modoapi.thread.GetErrorCodeThread;

import java.util.HashMap;
import java.util.Map;

public class ErrorCode {

    public static final String RESPONSE_BAD_REQ                     = "E1004";
    public static final String RESPONSE_FIRST_NAME_REQUIRED         = "E1006";
    public static final String RESPONSE_EMAIL_REQUIRED              = "E1007";
    public static final String RESPONSE_BAD_EMAIL                   = "E1008";
    public static final String RESPONSE_LAST_NAME_REQUIRED          = "E1009";
    public static final String RESPONSE_PASSWORD_REQUIRED           = "E1010";
    public static final String ACCESS_TOKEN_FACEBOOK_REQUIRED       = "E1011";
    public static final String CANNOT_GET_FACEBOOK_PROFILE          = "E1012";
    public static final String ACCESS_TOKEN_GOOGLE_REQUIRED         = "E1013";
    public static final String CANNOT_GET_GOOGLE_PROFILE            = "E1014";
    public static final String INVALID_EMAIL                        = "E1015";
    public static final String INVALID_PASSWORD                     = "E1016";
    public static final String BAD_REQUEST                          = "E1017";
    public static final String SERVER_ERROR                         = "E1018";
    public static final String BAD_REQUEST_HEADER                   = "E1019";
    public static final String NULL_REQUEST_BODY                    = "E1020";
    public static final String BAD_PHONE_NUM                        = "E1021";
    public static final String BAD_PASSWORD                         = "E1022";
    public static final String NULL_PHONE_NUMBER                    = "E1023";
    public static final String NULL_PASSWORD                        = "E1024";
    public static final String BAD_BODY_JSON                        = "E1025";
    public static final String NULL_EMAIL                           = "E1026";
    public static final String BAD_EMAIL                            = "E1027";
    public static final String NULL_USERNAME                        = "E1027";
    public static final String NULL_OLD_PASS                        = "E1028";
    public static final String NULL_NEW_PASS                        = "E1029";

    public static final String BAD_USERNAME                         = "E1030";
    public static final String BAD_OLD_PASS                         = "E1031";
    public static final String BAD_NEW_PASS                         = "E1032";

    public static final String NULL_OTP                             = "E1033";
    public static final String BAD_OTP                              = "E1034";
    public static final String FILE_TOO_BIG                         = "E1035";

    public static String getMessage(String code, int language) {
        return GetErrorCodeThread.errorCodeMessage.get(code + "_" + language);
    }

 }
