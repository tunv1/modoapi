package com.mfs.modoapi.util;

import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

@WebFilter(filterName = "MyFilter", urlPatterns = {"/*"})
public class CommonFilter implements Filter {

    private static final Logger logger = Logger.getLogger(CommonFilter.class);


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {


    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {

        filterChain.doFilter(request, response);
        response.setCharacterEncoding("UTF-8");
    }

    @Override
    public void destroy() {

    }
}
