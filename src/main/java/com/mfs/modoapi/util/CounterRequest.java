package com.mfs.modoapi.util;

import java.util.concurrent.atomic.AtomicLong;

public class CounterRequest {
    private final static AtomicLong sessionCount = new AtomicLong(0);
    private final static AtomicLong requestCount = new AtomicLong(0);
    private final static AtomicLong responseCount = new AtomicLong(0);

    public static void sessionCreated() {
        sessionCount.incrementAndGet();
        requestCount.incrementAndGet();
    }

    public static void sessionDestroy() {

        sessionCount.decrementAndGet();
        responseCount.incrementAndGet();
    }

    public static void resetRequestCount() {
        requestCount.set(0);
    }

    public static void resetResponseCount() {
        requestCount.set(0);
    }

    public static long getTotalSessionCount() {
        return sessionCount.get();
    }

    public static long getTotalRequestCount() {
        return requestCount.get();
    }

    public static long getTotalResponseCount() {
        return responseCount.get();
    }
}
