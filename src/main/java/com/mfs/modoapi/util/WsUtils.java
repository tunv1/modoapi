/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mfs.modoapi.util;

import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

/**
 *
 * @author PHANNKVOS
 */
public class WsUtils {

    /**
     * process response from CoreWS
     *
     * @param res
     * @return
     */
    
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(WsUtils.class);
    
    public static Response readResult(Response res)  {
        if(res.getStatus()==200){
        return Response.status(res.getStatus()).entity(res.readEntity(String.class)).build();
        } else
           return Response.status(600).entity(res.readEntity(String.class)).build(); 
    }

    public static Response readResultNull(Response res) {
        JsonObject json = new JsonObject();
        if (res.getStatus() == 200) {
            
            String str = res.readEntity(String.class);

            if(str == null){
                json.addProperty("isSuccess", Boolean.TRUE);
                return Response.status(res.getStatus()).entity(json.toString()).build();
            }else{
                json = new Gson().fromJson(str, JsonObject.class);
                if(json.isJsonNull()){
                    json.addProperty("isSuccess", Boolean.TRUE);
                    return Response.status(res.getStatus()).entity(json.toString()).build();
                }
                return Response.status(res.getStatus()).entity(str).build();
            }
        }
        return Response.status(600).entity(res.readEntity(String.class)).build();
    }

}
