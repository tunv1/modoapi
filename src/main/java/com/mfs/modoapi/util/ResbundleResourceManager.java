package com.mfs.modoapi.util;

import java.util.ResourceBundle;

public class ResbundleResourceManager {

    private static ResourceBundle bundle;

    public ResbundleResourceManager() {
        if (bundle == null) {
            bundle = ResourceBundle.getBundle("cas");
        }
    }

    private static ResourceBundle getResourceBundle() {
        if (bundle == null) {
            bundle = ResourceBundle.getBundle("cas");
        }
        return bundle;

    }

    public static String getUserCoreWs() {
        return getResourceBundle().getString("USER_CORE_WS");
    }

    public static String getPassCoreWs() {
        return getResourceBundle().getString("PASS_CORE_WS");
    }

    public static String getUriCore() {
        return getResourceBundle().getString("URI_CORE");
    }

    public static String getMediaServerUrl() {
        return getResourceBundle().getString("URI_MEDIA_SERVER");
    }

    public static int getTimeoutConnectCoreWs() {
        return  Integer.parseInt(getResourceBundle().getString("TIMEOUT_CONNECT_CORE_WS"));
    }

    public static int getTimeoutConnectMultipartServer() {
        return  Integer.parseInt(getResourceBundle().getString("TIMEOUT_MULTIPART_SERVER_CONNECT"));
    }

    public static int getTimeoutReadMultipartServer() {
        return  Integer.parseInt(getResourceBundle().getString("TIMEOUT_MULTIPART_SERVER_READ"));
    }

    public static int getTimeoutReadCoreWs() {
        return Integer.parseInt(getResourceBundle().getString("TIMEOUT_READ_CORE_WS"));
    }

    public static int getMaxFileSize() {
        return Integer.parseInt(getResourceBundle().getString("MAX_FILE_SIZE"));
    }

    public static String getResource(String key) {
        return getResourceBundle().getString(key);
    }
}
