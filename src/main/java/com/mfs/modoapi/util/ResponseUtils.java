package com.mfs.modoapi.util;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.mfs.modoapi.util.exception.CommonException;
import com.mfs.modoapi.util.exception.CoreServiceException;
import com.mfs.modoapi.util.exception.RequiredFieldException;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.core.Response;

public class ResponseUtils {

    static JsonObject jsonObject = new JsonObject();
    private static final Logger logger = Logger.getLogger(ResponseUtils.class);
    static JsonObject err = new JsonObject();

    public static Response BadRequestException(BadRequestException badRequestException, int language) {
        jsonObject.addProperty("status", false);
        err.addProperty("code", ErrorCode.RESPONSE_BAD_REQ);
        err.addProperty("message", ErrorCode.getMessage(ErrorCode.RESPONSE_BAD_REQ, language));
        jsonObject.add("error", err);
        return Response.status(200).entity(jsonObject.toString()).build();
    }

    public static Response RequiredFieldException(RequiredFieldException requiredFieldException, int language) {
        jsonObject.addProperty("status", false);
        err.addProperty("code", requiredFieldException.getErrCode());
        err.addProperty("message", ErrorCode.getMessage(requiredFieldException.getErrCode(), language));
        jsonObject.add("error", err);
        return Response.status(200).entity(jsonObject.toString()).build();
    }

    public static Response CoreServiceException(CoreServiceException commonErrException, int language) {
        jsonObject.addProperty("status", false);
        err.addProperty("code", commonErrException.getErrCode());
        err.addProperty("message", commonErrException.getMessage());
        jsonObject.add("error", err);

        return Response.status(200).entity(jsonObject.toString()).build();
    }

    public static Response CommonException(CommonException commonErrException, int language) {
        jsonObject.addProperty("status", false);
        err.addProperty("code", commonErrException.getCode());
        err.addProperty("message", ErrorCode.getMessage(commonErrException.getCode(), language));
        jsonObject.add("error", err);
        return Response.status(200).entity(jsonObject.toString()).build();
    }


    public static Response BadRequest(int language, int seqId, String errCode, HttpServletRequest request) {
        JsonObject jsonObject = new JsonObject();
        JsonObject err = new JsonObject();
        jsonObject.addProperty("status", false);
        err.addProperty("code", errCode);
        err.addProperty("message", ErrorCode.getMessage(errCode, language));
        jsonObject.add("error", err);
        Response response = Response.status(200).entity(jsonObject.toString()).build();

        logger.info("SeqId = " + seqId + "| Response for Request = " + request.getRequestURI() + "| Response = " + response.getEntity().toString());

        return response;
    }

    public static Response Exception(int language) {

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("status", false);
        JsonObject err = new JsonObject();
        err.addProperty("code", 500);
        err.addProperty("message", ErrorCode.getMessage(ErrorCode.SERVER_ERROR, language));
        jsonObject.add("error", err);
        return Response.status(200).entity(jsonObject.toString()).build();
    }

    public static Response OK(int language, JsonObject joFromCore, String dataModel, Response resApi) {

        JsonObject jsonObject = new JsonObject();
        JsonObject err = new JsonObject();
        jsonObject.add("data", joFromCore.get(dataModel));
        jsonObject.addProperty("status", true);
        jsonObject.add("error", null);
        Response response = Response.status(200).entity(jsonObject.toString()).build();
        return response;
    }

    public static Response CoreServiceProblem(JsonObject jo, int seqId, HttpServletRequest request) {
        JsonObject jsonObject = new JsonObject();
        JsonObject err = new JsonObject();

        jsonObject.addProperty("status", false);
        err.addProperty("code", jo.get("rscode").getAsString());
        err.addProperty("message", jo.get("rstext").getAsString());
        jsonObject.add("error", err);
        Response response = Response.status(200).entity(jsonObject.toString()).build();

        logger.info("SeqId = " + seqId + "| Response for Request = " + request.getRequestURI() + "| Response = " + response.getEntity().toString());

        return response;
    }
}
