package com.mfs.modoapi.util;

import com.mfs.modoapi.thread.GetErrorCodeThread;
import org.apache.log4j.PropertyConfigurator;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.io.File;

@WebListener("application context listener")
public class ContextListener implements ServletContextListener {

    /**
     * Initialize log4j when the application is being started
     */
    @Override
    public void contextInitialized(ServletContextEvent event) {
        // initialize log4j here
        ServletContext context = event.getServletContext();
        String log4jConfigFile = context.getInitParameter("log4j-config-location");
        String fullPath = context.getRealPath("") + File.separator + log4jConfigFile;

        PropertyConfigurator.configure(fullPath);

        GetErrorCodeThread getErrorCodeThread = new GetErrorCodeThread();
        getErrorCodeThread.setName("getErrorCodeThread");
        getErrorCodeThread.execute();

    }

    @Override
    public void contextDestroyed(ServletContextEvent event) {
        System.out.println("Server is stopping");
    }
}
