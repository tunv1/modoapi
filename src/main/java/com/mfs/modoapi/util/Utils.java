package com.mfs.modoapi.util;

import com.google.gson.JsonObject;

import javax.ws.rs.core.Response;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {
    private static final Pattern patternMsisdn = Pattern.compile("^(0|84|\\+84|)([3-9][0-9]{8})$"); // (x062634xx | 0x062634xx | 84x062634xx | +84x062634xx)

    public static String getMsisdn(String msisdn) {
        try {
            Matcher m = patternMsisdn.matcher(msisdn.trim()) ;
            if (m.find()) {
                return m.group(2);
            } else {
                return null;
            }
        } catch (NullPointerException ex) {
            return null;
        }
    }
    
    public static boolean isNullOrEmpty(String ... vars) {
        for (String var : vars) {
            if (var == null || var.trim().length() < 1) {
                return true;
            }
        }
        return false;
    }

    public static boolean isEmpty(String ... vars) {
        for (String var : vars) {
            if (var.trim().length() < 1) {
                return true;
            }
        }
        return false;
    }

    public static boolean isNull(Object ... vars) {
        for (Object var : vars) {
            if (var == null) {
                return true;
            }
        }
        return false;
    }

    public static boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false;
        }
        try {
            double d = Double.parseDouble(strNum);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    public static Response badRequest() {
        JsonObject jo = new JsonObject();
        jo.addProperty("message", "Please check you request");
        return Response.status(Response.Status.BAD_REQUEST).entity(jo.toString()).build();
    }

    public static Response ok(JsonObject jo) {
        return Response.status(Response.Status.OK).entity(jo.toString()).build();
    }

    public static Response internalServerError() {
        JsonObject jo = new JsonObject();
        jo.addProperty("message", "Sorry - Server Error Occured");
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(jo.toString()).build();
    }

    public static boolean checkEmailRegex(String email) {
        return email.matches("^[a-zA-Z0-9_.-]+@[a-zA-Z0-9.-]+$");
    }

    public static boolean checkPhoneRegex(String phone) {
        return phone.matches("^(84|0|)([0-9]{9})$");
    }

    public static boolean checkOtpRegex(String otp) {
        return true;
    }

    public static boolean checkPassword(String password) {
        return password.trim().length() > 5 && password.trim().length() < 101;
    }

    public static boolean checkResponseFromCore(JsonObject jsonObject) {
        return jsonObject.get("rscode") != null && (jsonObject.get("rscode").getAsString().trim().equals("E0000"));
    }

    public static boolean isLanguageValid(String language) {
        return (!Utils.isNullOrEmpty(language)) && ((language.trim().equals("en")) || (language.trim().equals("vi")));
    }

    public static int getLanguage(String language) {
        if (language.equals("en")) {
            return 1;
        } else {
            return 0;
        }
    }
}
