/*
* Copyright (C) 2016 Mobifone Service. All rights reserved.
* MOBIFONESERVICE/VAS. Use is subject to license terms.
 */
package com.mfs.modoapi.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import io.jsonwebtoken.*;
import java.security.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author PHANNKVOS
 */
public class Token {

    static JSONParser jsonParser = new JSONParser();

     static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(Token.class);

    public static String genAccessToken(String keySid, String keySecret,
                                        int expireInSecond, String username) throws Exception{

        Algorithm algorithmHS = Algorithm.HMAC256(keySecret);

        Map<String, Object> headerClaims = new HashMap<>();
        headerClaims.put("typ", "JWT");
        headerClaims.put("alg", "HS256");
        headerClaims.put("cty", "stringee-api;v=1");

        long exp = (System.currentTimeMillis()) + expireInSecond * 1000L;

        return JWT.create()
                .withHeader(headerClaims)
                .withClaim("jti", keySid + "-" + System.currentTimeMillis())
                .withClaim("iss", keySid)
                .withClaim("userId", username)
                // thử bỏ expire date tunv 2022-03-30
                .withExpiresAt(new Date(exp))
                .sign(algorithmHS);

    }

    public static String getUsernameFromToken(String token, long seqId) throws JWTVerificationException {
        Algorithm algorithmHS = Algorithm.HMAC256(Constants.SECRET_KEY_MFS);
        DecodedJWT jwt = JWT.decode(token) ;

        algorithmHS.verify(jwt);
        return jwt.getClaim("userId").asString() ;
    }


    public static void main(String[] args) throws Exception {

        System.out.println(genAccessToken("AC43cd0f4f6c9c9d71152465e4e95b4167", "fd4d91bbc809c5eb88b53f6824e97db1",
                1000000, "chao"));

    }

}
