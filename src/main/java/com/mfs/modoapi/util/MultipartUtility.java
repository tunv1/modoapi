package com.mfs.modoapi.util;

import org.apache.log4j.Logger;

import java.io.*;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MultipartUtility {
    private static final Logger logger = Logger.getLogger(MultipartUtility.class);

    private final String boundary;
    private static final String LINE_FEED = "\r\n";
    private HttpURLConnection httpConn;
    private String charset;
    private OutputStream outputStream;
    private PrintWriter writer;

    /**
     * This constructor initializes a new HTTP POST request with content type
     * is set to multipart/form-data
     */
    public MultipartUtility(String requestURL, String charset, int seqId)
            throws IOException {
        this.charset = charset;

        // creates a unique boundary based on time stamp and sequence ID
        boundary = "--" + System.currentTimeMillis() + "" + seqId;

        URL url = new URL(requestURL);
        httpConn = (HttpURLConnection) url.openConnection();
        httpConn.setUseCaches(false);
        httpConn.setDoOutput(true); // indicate POST method
        httpConn.setDoInput(true);
        httpConn.setReadTimeout(ResbundleResourceManager.getTimeoutReadMultipartServer());
        httpConn.setConnectTimeout(ResbundleResourceManager.getTimeoutConnectMultipartServer());
        httpConn.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
        httpConn.setRequestProperty("Connection", "keep-alive");
        outputStream = httpConn.getOutputStream();
        writer = new PrintWriter(new OutputStreamWriter(outputStream, charset), true);
    }

    /**
     * Adds a form field to the request
     * @param name field name
     * @param value field value
     */
    public void addFormField(String name, String value) {
        writer.append("--" + boundary).append(LINE_FEED);
        writer.append("Content-Disposition: form-data; name=\"" + name + "\"").append(LINE_FEED);
        writer.append(LINE_FEED);
        writer.append(value).append(LINE_FEED);
        writer.flush();
    }

    /**
     * Add a file to the request
     * @param fieldName
     * @param inputStream
     * @param fileName
     * @param contentType
     * @throws IOException
     */
    public void addFilePart(String fieldName, BufferedInputStream inputStream, String fileName, String contentType)
            throws IOException {
        writer.append("--" + boundary).append(LINE_FEED);
        writer.append( "Content-Disposition: form-data; name=\"" + fieldName + "\"; filename=\"" + fileName + "\"") .append(LINE_FEED);
        writer.append("Content-Type: " + contentType).append(LINE_FEED);
        writer.append(LINE_FEED);
        writer.flush();

        byte[] buffer = new byte[4096];
        int bytesRead;
        while ((bytesRead = inputStream.read(buffer)) != -1) {
            outputStream.write(buffer, 0, bytesRead);
        }
        outputStream.flush();
        inputStream.close();

        writer.append(LINE_FEED);
        writer.flush();
    }

    /**
     * Adds a header field to the request.
     * @param name - name of the header field
     * @param value - value of the header field
     */
    public void addHeaderField(String name, String value) {
        writer.append(name + ": " + value).append(LINE_FEED);
        writer.flush();
    }

    /**
     * Completes the request and receives response from the server.
     */
    public String finish() throws IOException {
        List<String> response = new ArrayList<String>();
        writer.append(LINE_FEED).flush();
        writer.append("--" + boundary + "--").append(LINE_FEED);
        writer.flush();
        writer.close();

        // checks server's status code first
        StringBuilder stringBuilder = new StringBuilder();

        InputStream inputStream = null;
        if (httpConn.getResponseCode() == HttpURLConnection.HTTP_OK) {
            inputStream = httpConn.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader( inputStream));
            String line = null;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
            }
            reader.close();
        }

        httpConn.disconnect();
        return stringBuilder.toString();
    }

}