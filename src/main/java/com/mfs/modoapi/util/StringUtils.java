package com.mfs.modoapi.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class StringUtils {
    public static String UrlEncode(String s) {
        try {
            return URLEncoder.encode(s, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            return s;
        }
    }
}
