package com.mfs.modoapi.util.exception;

import com.mfs.modoapi.util.ErrorCode;

public class RequiredFieldException extends Exception{

    String errCode;

    public RequiredFieldException(String errCode) {
        super("");
        this.errCode = errCode;
    }

    public String getErrCode() {
        return errCode;
    }

    public void setErrCode(String errCode) {
        this.errCode = errCode;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("RequiredFieldException{");
        sb.append("errCode='").append(errCode).append('\'');
        sb.append(", message='").append(ErrorCode.getMessage(errCode, 1)).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
