package com.mfs.modoapi.util.exception;

import com.mfs.modoapi.util.ErrorCode;

public class CoreServiceException extends Exception{
    String errCode;
    String message;

    public CoreServiceException(String message, String errCode, String message1) {
        super(message);
        this.errCode = errCode;
        this.message = message1;
    }

    public String getErrCode() {
        return errCode;
    }

    public void setErrCode(String errCode) {
        this.errCode = errCode;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("CoreServiceException{");
        sb.append("errCode='").append(errCode).append('\'');
        sb.append(", message='").append(message).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
