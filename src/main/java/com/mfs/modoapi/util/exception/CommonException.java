package com.mfs.modoapi.util.exception;

import com.mfs.modoapi.util.ErrorCode;

public class CommonException extends Exception{

    String code;

    public CommonException(String code) {
        super("");
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("CommonException{");
        sb.append("code='").append(code).append('\'');
        sb.append(", message='").append(ErrorCode.getMessage(code, 1)).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
