package com.mfs.modoapi.thread;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mfs.modoapi.util.GeneratorSeq;
import com.mfs.modoapi.util.Utils;
import com.mfs.modoapi.util.WsUtils;
import com.mfs.modoapi.ws.CoreGetErrorCode;
import org.apache.log4j.Logger;

import javax.ws.rs.core.Response;
import java.util.concurrent.ConcurrentHashMap;

public class GetErrorCodeThread  extends ActionThread {

    public  static  ConcurrentHashMap<String, String> errorCodeMessage = new ConcurrentHashMap<>();


    private static final Logger logger = Logger.getLogger(GetErrorCodeThread.class);

    @Override
    protected void onExecuting() throws Exception {
        logger.info(this.getName() + " execute");

    }

    @Override
    protected void onKilling() {
        this.kill();
    }

    @Override
    protected void onException(Exception e) {
        logger.error("", e);
    }

    @Override
    protected long sleeptime() throws Exception {
        // 1 minute
        return 60000;
    }

    @Override
    protected void action() {
        try {


            CoreGetErrorCode coreGetErrorCode = new CoreGetErrorCode();
            int seqId = GeneratorSeq.getNextSeq();
            Response resApi = WsUtils.readResult(coreGetErrorCode.getErrorCode(seqId));
            JsonObject joResFromCore = new Gson().fromJson(resApi.getEntity().toString(), JsonObject.class);
            logger.info("SeqId = " + seqId + "| Response from Core service = " + joResFromCore.toString().replaceAll("\r\n", ""));

            if (Utils.checkResponseFromCore(joResFromCore)) {
                if (joResFromCore.get("errorCodes") != null) {
                    if (joResFromCore.get("errorCodes").getAsJsonArray() != null) {
                        JsonArray jsonArrayErrCodes = new Gson().fromJson(joResFromCore.get("errorCodes").getAsJsonArray(), JsonArray.class);

                        if (jsonArrayErrCodes.size() > 0) {
                            errorCodeMessage.clear();
                            for (JsonElement jo : jsonArrayErrCodes) {
                                errorCodeMessage.put(jo.getAsJsonObject().get("code").getAsString() + "_" + jo.getAsJsonObject().get("languageId"),
                                        jo.getAsJsonObject().get("message").getAsString());
                            }
                            logger.info("SeqId = " + seqId + "| Total error code loaded: " + errorCodeMessage.size());

                        }

                    } else {
                        logger.info("SeqId = " + seqId + "| Bad errorCodes array" );
                    }

                } else {
                    logger.info("SeqId = " + seqId + "| errorCodes null!" );
                }
            } else {
                logger.info("SeqId = " + seqId + "| Bad core service response" );

            }



        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
